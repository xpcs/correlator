# Correlator

This is the base library for XPCS calculations. It mainly provides tools for computing

```math
g_2 (q, \tau) = \dfrac{
\langle \langle I(t, p) I(t + \tau, p) \rangle_p \rangle_t
}{
\langle \langle I(t, p) \rangle_p \langle I(t+\tau, p) \rangle_p \rangle_t
}
```