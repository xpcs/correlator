# This script is used to list the datasets in the main folder and print information about a specific dataset.
# The script can be run with the --info argument to print information about a specific dataset.

import argparse
import os

import yaml


def list_datasets(main_folder):
    """
    List all datasets in the main folder.
    """
    try:
        datasets = sorted([d for d in os.listdir(main_folder) if os.path.isdir(os.path.join(main_folder, d))])
        print("\033[96mDatasets in the main folder:\033[0m")
        for idx, dataset in enumerate(datasets, start=1):
            print(f"\033[96m{(str(idx)+'.').ljust(4)}\033[0m {dataset}")
    except Exception as e:
        print(f"Error listing datasets: {e}")


def print_dataset_info(dataset_folder):
    """
    Print information from the YAML file and list the names of the files in the data, qmask, raw_data, and reference folders.
    """
    try:
        # Load YAML file
        yaml_file_path = os.path.join(dataset_folder, "info.yaml")
        with open(yaml_file_path) as yaml_file:
            dataset_info = yaml.safe_load(yaml_file)

        print("\033[96mDataset Information:\033[0m")
        for key, value in dataset_info.items():
            print(f"{key}: {value}")

        # List files in subfolders
        subfolders = ["data", "qmask", "raw_data", "reference"]
        for subfolder in subfolders:
            subfolder_path = os.path.join(dataset_folder, subfolder)
            if os.path.exists(subfolder_path):
                files = os.listdir(subfolder_path)
                print(f"\033[96m\nFiles in {subfolder} folder:\033[0m")
                for file in files:
                    print(file)
            else:
                print(f"\033[96m\n{subfolder} folder does not exist.\033[0m")
    except Exception as e:
        print(f"Error reading dataset info: {e}")


# Example usage
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process some datasets.")
    parser.add_argument("--info", type=str, metavar="dataset_name", help="Print information about a specific dataset")
    args = parser.parse_args()

    main_folder = "/data/projects/xpcs_repository/correlator_test_data"

    if args.info:
        dataset_folder = os.path.join(main_folder, args.info)
        print_dataset_info(dataset_folder)
    else:
        list_datasets(main_folder)
