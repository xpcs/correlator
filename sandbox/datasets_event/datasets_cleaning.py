# This was run only once to coppy the datasets to the new location

import os
import shutil

import yaml

# Define the datasets dictionary
datasets = {
    "dataset02": {
        "raw_data_fname": "/data/id10/inhouse/software/dynamix/datasets/dataset02/scan0005/eiger1_0000.h5",
        "data_fname": "/scisoft/dynamix/data/dataset02/xpcs_010000.npz",
        "qmask_fname": "/data/id10/inhouse/software/dynamix/datasets/dataset02/analysis/scan0005_0_10000/DUKE_qmask.npy",
        "reference_fname": "/scisoft/dynamix/data/dataset02/reference.npz",
        "n_frames_for_test": 10000,
        "test_tolerance": {
            "g2": 1.3e-4,
            "std": 1e-7,
            "denom": 3e-3,
        },
        "comments": "dataset02: 10k frames. Not really sparse in the ROI",
    },
    "dataset03": {
        "raw_data_fname": "/data/id10/inhouse/software/dynamix/datasets/dataset03/scan0002/eiger4m_0000.h5",
        "data_fname": "/scisoft/dynamix/data/dataset03/xpcs_040000.npz",
        "qmask_fname": "/data/id10/inhouse/software/dynamix/datasets/dataset03/analysis/Pt1_10GPa_2_242C/scan0002_0_5000/Pt1_10GPa_2_242C_qmask.npy",
        "reference_fname": "/scisoft/dynamix/data/dataset03/reference.npz",
        "n_frames_for_test": 40000,
        "test_tolerance": {
            "g2": 1.3e-4,
            "std": 1e-7,
            "denom": 3e-3,
        },
        "comments": "dataset03: 40k frames",
    },
    "dataset04": {
        "raw_data_fname": "/scisoft/dynamix/data/dataset04/Vit4_0GPa_Tg_m_30_Monday_25p0_575K_00001_merged.h5",
        "data_fname": "/scisoft/dynamix/data/dataset04/xpcs_200000.npz",
        "qmask_fname": "/data/id10/inhouse/software/dynamix/datasets/dataset04/analysis/Vit4_0GPa_Tg_m_30_Monday_25p0_575K/00001_0_200000/Vit4_0GPa_Tg_m_30_Monday_25p0_575K_qmask.npy",
        "reference_fname": "/scisoft/dynamix/data/dataset04/reference.npz",
        "n_frames_for_test": 20000,
        "test_tolerance": {
            "g2": 2e-4,
            "std": 2.2e-7,
            "denom": 1e-4,
        },
        "comments": "dataset04: 200k frames",
    },
    "dataset05": {
        "raw_data_fname": "/scisoft/dynamix/data/dataset05/dataset05_merged.h5",
        "data_fname": "/scisoft/dynamix/data/dataset05/xpcs_1200000.npz",
        "qmask_fname": "/scisoft/dynamix/data/dataset05/qmask_dummy.npy",
        "reference_fname": "/scisoft/dynamix/data/dataset05/reference.npz",
        "n_frames_for_test": 20000,
        "test_tolerance": {
            "g2": 1e-5,
            "std": 1e-7,
            "denom": 1e-4,
        },
        "comments": "dataset05: 1.2M frames",
    },
}

# Add dataset01 scans
for i in range(7, 13 + 1):
    datasets[f"dataset01_scan{i:04d}"] = {
        "raw_data_fname": f"/scisoft/dynamix/data/dataset01_scan{i:04d}/scan{i:04d}_merged.h5",
        "data_fname": f"/scisoft/dynamix/data/dataset01_scan{i:04d}/xpcs_020000.npz",
        "qmask_fname": "/scisoft/dynamix/data/dataset01_scan0010/SiO2-21p67keV_qmask_pp.npy",
        "reference_fname": f"/scisoft/dynamix/data/dataset01_scan{i:04d}/reference.npz",
        "n_frames_for_test": 20000,
        "test_tolerance": {
            "g2": 1e-5,
            "std": 1e-7,
            "denom": 1e-4,
        },
        "comments": "dataset01: 20k frames, sparse, WAXS geometry - some pixels have high data value",
    }

# Adapt test tolerance to some datasets
datasets["dataset01_scan0007"]["test_tolerance"]["denom"] = 2e-4
datasets["dataset01_scan0008"]["test_tolerance"]["g2"] = 2e-4
datasets["dataset01_scan0013"]["test_tolerance"]["g2"] = 1.3e-4

# Create the main folder
main_folder = "/data/projects/xpcs_repository/correlator_test_data"
os.makedirs(main_folder, exist_ok=True)
print(f"Created main folder: {main_folder}")


# Function to create subfolders and copy files
def create_dataset_folders(dataset_name, dataset_info):
    # Format the dataset folder name
    if dataset_name.startswith("dataset01_scan"):
        dataset_folder_name = dataset_name.replace("dataset01_scan", "dataset_0001_scan_")
    else:
        dataset_folder_name = dataset_name.replace("dataset", "dataset_00")
    dataset_folder = os.path.join(main_folder, dataset_folder_name)
    os.makedirs(dataset_folder, exist_ok=True)
    print(f"Created dataset folder: {dataset_folder}")

    subfolders = ["data", "raw_data", "qmask", "reference"]
    for subfolder in subfolders:
        os.makedirs(os.path.join(dataset_folder, subfolder), exist_ok=True)
        print(f"Created subfolder: {os.path.join(dataset_folder, subfolder)}")

    # Copy files to respective subfolders
    shutil.copy(dataset_info["raw_data_fname"], os.path.join(dataset_folder, "raw_data"))
    print(f"Copied raw data file to: {os.path.join(dataset_folder, 'raw_data')}")

    shutil.copy(dataset_info["data_fname"], os.path.join(dataset_folder, "data"))
    print(f"Copied data file to: {os.path.join(dataset_folder, 'data')}")

    shutil.copy(dataset_info["qmask_fname"], os.path.join(dataset_folder, "qmask"))
    print(f"Copied qmask file to: {os.path.join(dataset_folder, 'qmask')}")

    shutil.copy(dataset_info["reference_fname"], os.path.join(dataset_folder, "reference"))
    print(f"Copied reference file to: {os.path.join(dataset_folder, 'reference')}")

    # Create YAML file with dataset information
    yaml_data = {
        "raw_data_fname": dataset_info["raw_data_fname"],
        "data_fname": dataset_info["data_fname"],
        "qmask_fname": dataset_info["qmask_fname"],
        "reference_fname": dataset_info["reference_fname"],
        "n_frames_for_test": dataset_info["n_frames_for_test"],
        "test_tolerance": dataset_info["test_tolerance"],
        "comments": dataset_info["comments"],
    }

    yaml_file_path = os.path.join(dataset_folder, "info.yaml")
    with open(yaml_file_path, "w") as yaml_file:
        yaml.dump(yaml_data, yaml_file, default_flow_style=False)
    print(f"Created YAML file: {yaml_file_path}")


# Create folders and copy files for each dataset
for dataset_name, dataset_info in datasets.items():
    print(f"Processing dataset: {dataset_name}")
    create_dataset_folders(dataset_name, dataset_info)
    print(f"Finished processing dataset: {dataset_name}")

print("All datasets processed successfully.")
