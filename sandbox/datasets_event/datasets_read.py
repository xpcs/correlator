import os
import pprint

import yaml


class DatasetReader:
    """

    Class to read datasets from a main directory of datsets for tests

    """

    def __init__(self, main_directory):
        self.main_directory = main_directory

    def _read_dataset(self, dataset_name):
        dataset_path = os.path.join(self.main_directory, dataset_name)
        if not os.path.isdir(dataset_path):
            raise NotADirectoryError(f"{dataset_path} is not a directory")
        yaml_file_path = os.path.join(dataset_path, "info.yaml")
        if not os.path.exists(yaml_file_path):
            raise FileNotFoundError(f"{yaml_file_path} does not exist")
        with open(yaml_file_path) as yaml_file:
            dataset_info = yaml.safe_load(yaml_file)
            # No we need to change here that the original path is replaced by the
            # path to the files in the subdirectories of the new main directory
            for key in dataset_info:
                if key in ["data_fname", "qmask_fname", "raw_data_fname", "reference_fname"]:
                    subfolder = {
                        "data_fname": "data",
                        "qmask_fname": "qmask",
                        "raw_data_fname": "raw_data",
                        "reference_fname": "reference",
                    }[key]
                    filename = os.path.basename(dataset_info[key])
                    dataset_info[key] = os.path.join(self.main_directory, dataset_name, subfolder, filename)
            return dataset_info

    def __getitem__(self, dataset_name):
        return self._read_dataset(dataset_name)


if __name__ == "__main__":
    main_directory = "/data/projects/xpcs_repository/correlator_test_data"
    dataset_reader = DatasetReader(main_directory)
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(dataset_reader["dataset_0002"])
