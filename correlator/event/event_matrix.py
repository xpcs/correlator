from math import ceil, log10

import numpy as np
import pyopencl.array as parray
from pyopencl.elementwise import ElementwiseKernel

from ..base import CorrelationResult
from ..opencl.correlator import OpenclCorrelator
from ..utils import tinker_ttcf_diagonal, tinker_ttcf_diagonal_line_layout, updiv

"""
Correlators that build the full correlation matrix ("two-times-correlation function"),
from either space-compact data (SMatrixEventCorrelator), or time-compact data (TMatrixEventCorrelator)
"""


class MatrixEventCorrelatorBase(OpenclCorrelator):
    """
    Base class for MatrixEventCorrelator.
    It also provides entry points for both implementations.
    """

    # available_binning_functions = {"log": 1, "log2": 2, "log10": 3, "arcsinh": 4}
    available_binning_functions = {"log10": 3}
    _log_binning_points_per_decade = 10
    _other_compilation_flags = []

    def __init__(
        self,
        n_frames,
        qmask,
        dtype=np.uint8,
        binning=None,
        scale_factors=None,
        horizontal_binning=None,
        extra_options=None,
        **openclprocessing_kwargs,
    ):
        """
        Initialize a TTCF (two-times correlation function) builder.
        Please refer to the documentation of 'BaseCorrelator'.

        Other parameters
        ----------------
        binning: int, optional
            If set, this is the vertical binning factor for TTCF. Please refer to the "Notes" section below for more details.
        horizontal_binning: str, optional
            Configure the TTCF binning function in the horizontal direction. Default is no horizontal binning.
            The function can be "log10", "log2", "log", "arcsinh"
            The number of points per decade can be configured via 'extra_options'

        Notes
        -----
        When the number of frames is large, it's too expensive memory-wise to build the full correlation matrix (or even half of it).
        To circumvent this, a binning can be applied vertically and/or horizontally on the TTCF.
        When using binning, the result r(t1, t2) = sum(I(t1, :) * I(t2, :)) is accumulated into
            TTCF[t1/binning, phi(t2 - t1)].
        where
           - 'binning' is a integer defining the binning in the vertical direction
           - phi() is a function defining the binning in the horizontal direction.
        Note that the output data layout is different when using either.
        """
        self._setup_binning(binning, horizontal_binning)
        super().__init__(
            n_frames,
            qmask,
            dtype=dtype,
            scale_factors=scale_factors,
            extra_options=extra_options,
            **openclprocessing_kwargs,
        )

    def _init_finalize_cl(self):
        self._binning_pts_per_decade = self.extra_options.get(
            "binning_points_per_decade", self._log_binning_points_per_decade
        )
        if self.binning is not None:
            self._other_compilation_flags = ["-DDO_BINNING"]
        if self.horizontal_binning is not None:
            self._other_compilation_flags = [
                "-DDO_BINNING",
                "-DBINNING_FUNC=%d" % self.available_binning_functions[self.binning_function],
                "-DBINNING_PTS_PER_DECADE=%d" % self._binning_pts_per_decade,
            ]
        self._init_corr_matrix()
        self._setup_kernels()

    def _setup_binning(self, binning, horizontal_binning):
        self.binning_function = None
        self._output_layout = "flattened"
        if binning is None and horizontal_binning is not None:
            binning = 1
        self.binning = binning
        self.horizontal_binning = horizontal_binning

        if self.binning is None and self.horizontal_binning is None:
            return

        # Another layout is used when using binning
        self._output_layout = "columns"
        self.binning_function = self.horizontal_binning
        if self.binning_function is not None and self.binning_function not in self.available_binning_functions:
            raise ValueError(
                "Unsupported binning function '%s'. Available are: %s"
                % (self.binning_function, str(self.available_binning_functions))
            )

    def _configure_extra_options(self, extra_options):
        super()._configure_extra_options(extra_options)
        if self.binning_function is not None and (
            np.dtype(self.extra_options["res_dtype"]) != np.uint64
            or np.dtype(self.extra_options["sums_dtype"]) != np.uint64
        ):
            print("Warning: log-rebinning activated, setting integer intermediate results data type to 64 bits")
            self.extra_options["res_dtype"] = np.uint64
            self.extra_options["sums_dtype"] = np.uint64

    def _get_log_bins(self):
        if self.binning_function == "log10":
            self._log_bins = [
                binning_function_log10(i, self.n_frames, endpoint=False, npts_per_decade=self._binning_pts_per_decade)
                for i in range(self.n_frames)
            ]
        else:
            raise NotImplementedError

    def _init_corr_matrix(self):
        self.n_times = self.n_frames
        self.corr_function_length = self.n_frames
        if self._output_layout == "flattened":
            # Full correlation matrix, in flattened form (to store only half the elements)
            self.correlation_matrix_flat_size = self.n_frames * (self.n_frames + 1) // 2
            self._corr_matrix_shape = (self.n_bins, self.correlation_matrix_flat_size)
        else:
            # Correlation matrix in non-flattened form: (n_frames//binning, phi(n_frames))
            if self.binning_function is not None:
                self._get_log_bins()
                # phi = getattr(np, self.binning_function)
                # self.corr_function_length = int(phi(self.n_frames) * self._binning_pts_per_decade) + 1
                self.corr_function_length = self._log_bins[-1] + 1
            self._corr_matrix_shape = (self.n_bins, self.n_frames // (self.binning or 1), self.corr_function_length)
        self.d_corr_matrix = self.get_opencl_array("corr_matrix", self._corr_matrix_shape, dtype=self._res_dtype)
        self.d_sums_corr_matrix = self.get_opencl_array(
            "sums_corr_matrix", self._corr_matrix_shape, dtype=self._res_dtype
        )
        self.d_sums = self.get_opencl_array("sums", (self.n_bins, self.n_frames), dtype=self._res_dtype)
        self.cl_div = None
        # Compile/warm-up additional pyopencl kernels
        self.get_opencl_array("_d_tmp_warmup", (10,), dtype=self._offset_dtype)
        parray.max(parray.diff(self._d_tmp_warmup))

    def _correlate_sums(self):
        wg = None
        grid = (self.n_frames, self.n_frames, self.n_bins)
        kernel_args = [
            self.d_sums.data,
            self.d_sums_corr_matrix.data,
            np.int32(self.n_frames),
            np.int32(self.corr_function_length),
            np.int32(self.n_bins),
        ]
        if self.binning is not None:
            kernel_args.insert(-1, np.int32(self.binning))
        evt = self.build_scalar_correlation_matrix(self.queue, grid, wg, *kernel_args)
        evt.wait()
        self.profile_add(evt, "Correlate d_sums")

    def _get_normalized_ttcf_opencl(self, d_num, d_denom):
        if self.cl_div is None:
            normalized_ttcf = self.get_opencl_array("normalized_ttcf", d_num.shape, dtype=np.float32)
            if self.binning is not None:
                # When using binning, the TTCF matrix has zeros everywhere below the anti-diagonal. Avoid divisions in this zone.
                eltwise_operation = "uint y = i / n_frames; uint x = i - y*n_frames; if (binning_factor*y <= n_frames - x) res[i] = num[i] * 1.0f / denom[i]; else res[i] = 0;"
                kernel_signature = "float* res, %s* num, %s* denom, int n_frames, int binning_factor" % (
                    self._res_c_dtype,
                    self._res_c_dtype,
                )
                self._cl_div_other_call_args = [np.int32(self.n_frames), np.int32(self.binning)]
            else:
                # "res[i] = ((denom[i] > 0) ? (num[i] * 1.0f / denom[i]) : (0.0f))",
                eltwise_operation = "res[i] = num[i] * 1.0f / denom[i]"
                kernel_signature = "float* res, %s* num, %s* denom" % (self._res_c_dtype, self._res_c_dtype)
                self._cl_div_other_call_args = []
            self.cl_div = ElementwiseKernel(
                self.ctx,
                kernel_signature,
                eltwise_operation,
                "float_int_divide",
                preamble='#include "dtypes.h"',
                options=self._dtype_compilation_flags,
            )
        normalized_ttcf = self.get_opencl_array("normalized_ttcf", d_num.shape, dtype=np.float32)
        self.cl_div(normalized_ttcf, d_num, d_denom, *self._cl_div_other_call_args)
        return normalized_ttcf

    def _get_normalized_ttcf_numpy(self, num, denom):
        res = num.astype(np.float32)
        denom = denom.astype(np.float32)
        res /= denom
        return res

    def get_normalized_ttcf(self, calculate_on_device=False):
        """
        From the previously-computed numerator and denominator of TTCF,
        compute the normalized matrix, i.e num/denom (including scaling factors).

        Parameters
        ----------
        calculate_on_device: bool, optional
            Whether to perform computations on GPU. This needs an additional memory allocation,
            so it might be not suitable for large number of frames.

        Returns
        -------
        normalized_ttcf: numpy.ndarray
            A numpy array containing the normalized two-times correlation matrix.
            The resulting array has shape (n_bins, n_frames, n_frames), i.e there are n_bins matrices.
            Only the upper half of each matrix was computed.
        """
        num = self.d_corr_matrix
        denom = self.d_sums_corr_matrix
        if calculate_on_device:
            ttcf_allbins = self._get_normalized_ttcf_opencl(num, denom).get()
        else:
            num = num.get()
            denom = denom.get()
            ttcf_allbins = self._get_normalized_ttcf_numpy(num, denom)
        if self.binning is None:
            normalized_ttcf = np.zeros((self.n_bins, self.n_frames, self.n_frames), dtype="f")
            for bin_idx in range(self.n_bins):
                normalized_ttcf[bin_idx] = flat_to_square(ttcf_allbins[bin_idx], dtype="f")
        else:
            normalized_ttcf = np.zeros(self._corr_matrix_shape, dtype="f")
            for bin_idx in range(self.n_bins):
                normalized_ttcf[bin_idx] = ttcf_allbins[bin_idx]
        return normalized_ttcf

    def get_correlation_function(self):
        get_g2_and_std_kernel = self.kernels.get_kernel("get_g2_and_std_v1")

        d_std = self.get_opencl_array("std", (self.n_bins, self.corr_function_length), dtype=np.float64)
        d_g2 = self.get_opencl_array("g2", (self.n_bins, self.corr_function_length), dtype=np.float64)

        grid = (self.corr_function_length, self.n_bins)
        wg = None  # TODO tune for performances

        kernel_args = [
            self.d_corr_matrix.data,
            self.d_sums_corr_matrix.data,
            self.d_scale_factors.data,
            d_g2.data,
            d_std.data,
            np.int32(self.corr_function_length),
            np.int32(self.n_bins),
        ]
        if self.binning is not None:
            kernel_args.insert(-1, np.int32(self.binning))
        evt = get_g2_and_std_kernel(
            self.queue,
            grid,
            wg,
            *kernel_args,
        )
        evt.wait()
        self.profile_add(evt, "get_g2_and_std")

        return d_g2, d_std, self.d_corr_matrix.data, self.d_sums_corr_matrix.data

    def correlate(
        self, sparse_data, normalize_ttcf_on_device=False, return_numpy_arrays=True, tinker_diagonal=False, **kwargs
    ):
        """
        Build the two-times correlation matrix (TTCF) and g2 and std.

        Parameters
        ----------
        sparse_data: SpaceCompactedData or TimeCompactedData
            Data in compact format
        normalize_ttcf_on_device: bool, optional
            Whether to compute the final normalization step of TTCF on GPU.
            If set to True, this step will be faster, although will consume more memory.
        return_numpy_arrays: bool, optional
            Whether to return numpy arrays in the result, instead of pyopencl arrays.

        Returns
        -------
        res: CorrelationResult
            Structure with "g2" (1D correlation function for each bin), "std" (standard deviation curve for each bin),
            and stack of TTCF.
        """
        self.build_correlation_matrices(sparse_data, **kwargs)
        ttcf = self.get_normalized_ttcf(calculate_on_device=normalize_ttcf_on_device)
        g2, std, _, _ = self.get_correlation_function()

        if return_numpy_arrays:
            g2 = g2.get()
            std = std.get()

        if tinker_diagonal:
            update_function = tinker_ttcf_diagonal_line_layout if self.binning is not None else tinker_ttcf_diagonal
            [update_function(ttcf_arr, scale_factor=self.scale_factors[i + 1]) for i, ttcf_arr in enumerate(ttcf)]

        return CorrelationResult(correlation=g2, ttcf=ttcf, std=std)


class SMatrixEventCorrelator(MatrixEventCorrelatorBase):
    kernel_files = ["matrix_evtcorrelator.cl", "utils.cl"]

    """
    A class for computing the two-times correlation function (TTCF) from space-compacted data.
    """

    def _setup_kernels(self):
        self._setup_and_compile_opencl_kernels(other_compilation_flags=self._other_compilation_flags)
        self.build_correlation_matrix_kernel = self.kernels.get_kernel("build_correlation_matrix")
        self.build_scalar_correlation_matrix = self.kernels.get_kernel("build_scalar_correlation_matrix")

    def _get_max_space_nnz(self, offsets):
        if isinstance(offsets, parray.Array):
            return parray.max(parray.diff(offsets)).get()[()]
        else:
            return np.diff(offsets).max()

    def _get_compacted_qmask(self, pixel_indices, offsets):
        # compaction of qmask (duplicates information!)
        # qmask_compacted[pixel_compact_idx] give the associated q-bin
        qmask_compacted = []
        qmask1D = self.qmask.ravel()
        for frame_idx in range(self.n_frames):
            i_start = offsets[frame_idx]
            i_stop = offsets[frame_idx + 1]
            qmask_compacted.append(qmask1D[pixel_indices[i_start:i_stop]])
        qmask_compacted = np.hstack(qmask_compacted)
        return qmask_compacted
        #

    def build_correlation_matrices(self, s_sparse_data, max_space_nnz=None):
        """
        Build the following correlation matrices:
          - numerator: < I(t1, p) * I(t2, p) >_p
          - denominator: < I(t1, p) >_p * < I(t2, p) >_p
        Where < . >_p denotes summing over a given q-bin.
        """
        self._check_dtypes(s_sparse_data)
        d_s_sparse_data = self._data_to_cl_arrays(s_sparse_data)

        d_data = d_s_sparse_data.data
        d_pixel_indices = d_s_sparse_data.indices
        d_offsets = d_s_sparse_data.offsets

        max_space_nnz = self._get_max_space_nnz(d_offsets)

        self.d_corr_matrix.fill(0)
        self.d_sums.fill(0)
        wg = (512, 1)  # TODO tune
        grid = (updiv(max_space_nnz, wg[0]) * wg[0], self.n_frames)

        kernel_args = [
            d_data.data,
            d_pixel_indices.data,
            d_offsets.data,
            self.d_qmask.data,
            self.d_corr_matrix.data,
            self.d_sums.data,
            np.int32(self.n_frames),
            np.int32(self.corr_function_length),
        ]
        if self.binning is not None:
            kernel_args.insert(-2, np.int32(self.binning))

        evt = self.build_correlation_matrix_kernel(self.queue, grid, wg, *kernel_args)
        evt.wait()
        self.profile_add(evt, "Build matrix correlation")

        # Build denominator
        self._correlate_sums()

        return self.d_corr_matrix, self.d_sums_corr_matrix


class TMatrixEventCorrelator(MatrixEventCorrelatorBase):
    """
    A class for computing the two-times correlation function (TTCF) from time-compacted data.
    """

    kernel_files = ["matrix_evtcorrelator_time.cl", "utils.cl"]
    _default_max_time_nnz = 100

    def _setup_kernels(self, max_time_nnz=None):
        self._max_t_nnz = max_time_nnz or self._default_max_time_nnz
        self._setup_and_compile_opencl_kernels(
            other_compilation_flags=self._other_compilation_flags + ["-DMAX_EVT_COUNT=%d" % self._max_t_nnz]
        )
        self.build_correlation_matrix_kernel_times = self.kernels.get_kernel(
            "build_correlation_matrix_times_representation"
        )
        self.build_scalar_correlation_matrix = self.kernels.get_kernel("build_scalar_correlation_matrix")

    @staticmethod
    def _get_max_time_nnz(t_sparse_data):
        offsets = t_sparse_data.offsets
        if isinstance(offsets, parray.Array):
            return parray.max(parray.diff(offsets)).get()[()]
        else:  # np.ndarray
            return np.max(np.diff(offsets))

    def _update_kernel_max_nnz_if_needed(self, t_sparse_data):
        """
        Recompile the OpenCL kernel with updated MAX_EVENT_COUNT, if needed
        """
        new_max_t_nnz = self._get_max_time_nnz(t_sparse_data)
        if new_max_t_nnz > self._max_t_nnz:
            self._setup_kernels(max_time_nnz=new_max_t_nnz)

    def build_correlation_matrices(self, t_sparse_data):
        """
        Build the following correlation matrices:
          - numerator: < I(t1, p) * I(t2, p) >_p
          - denominator: < I(t1, p) >_p * < I(t2, p) >_p
        Where < . >_p denotes summing over a given q-bin.
        """
        self._check_dtypes(t_sparse_data)
        self._update_kernel_max_nnz_if_needed(t_sparse_data)
        d_t_sparse_data = self._data_to_cl_arrays(t_sparse_data)

        self.d_corr_matrix.fill(0)
        self.d_sums.fill(0)

        wg = None
        grid = tuple(self.shape[::-1])
        kernel_args = [
            d_t_sparse_data.times.data,
            d_t_sparse_data.data.data,
            d_t_sparse_data.offsets.data,
            self.d_qmask.data,
            self.d_corr_matrix.data,
            self.d_sums.data,
            np.int32(self.shape[1]),
            np.int32(self.shape[0]),
            np.int32(self.n_frames),
            np.int32(self.corr_function_length),
            np.int32(1),  # pre-sort
        ]
        if self.binning is not None:
            kernel_args.insert(-1, np.int32(self.binning))
        evt = self.build_correlation_matrix_kernel_times(self.queue, grid, wg, *kernel_args)
        evt.wait()
        self.profile_add(evt, "Build matrix correlation (times repr.)")

        # Build denominator
        self._correlate_sums()

        return self.d_corr_matrix, self.d_sums_corr_matrix


def flat_to_square(arr, shape=None, dtype=np.uint32):
    """
    Convert a flattened correlation "matrix" to a rectangular matrix.

    Paramaters
    ----------
    arr: numpy.ndarray
        1D array, flattened correlation matrix
    shape: tuple of int, optional
        Correlation matrix shape. If not given, resulting matrix is assumed square.
    """
    if shape is None:
        n2 = arr.size
        n = int(((1 + 8 * n2) ** 0.5 - 1) / 2)
        shape = (n, n)
    idx = np.triu_indices(shape[0], m=shape[1])
    res = np.zeros(shape, dtype=dtype)
    res[idx] = arr
    return res


def estimate_binning_factor(
    n_frames,
    n_bins,
    mem_GB,
    normalize_ttcf_on_device=False,
    sparse_data=None,
    results_in_double_precision=False,
    suggest_friendly_binning_factor=True,
):
    """
    Estimate how much the two-times correlation matrix has to be binned
    in order to compute it for 'n_frames'.

    Parameters
    ----------
    n_frames: int
        Number of frames
    n_bins: int
        Number of scattering vectors
    mem_GB: float
        Available device memory in GigaBytes
    normalize_ttcf_on_device: bool, optional
        Whether the TTCF normalization will take place on device. If set to True, it requires more memory
        (one additional matrix to store)
    sparse_data: TimeCompactedData or SpaceCompactedData, optional
        Object containing either space-compacted data or time-compacted data.
        If provided, the memory estimation will be more precise as it will account for the data (usually a few GB)
    results_in_double_precision: bool, optional
        Do the estimation assuming results are stored in double precision (8 Bytes) instead of simple precision (4 Bytes).
    suggest_friendly_binning_factor: bool, optional
        Whether to ensure that the returned binning factor divides the number of frames
    """
    n_matrices_to_store = 2
    if normalize_ttcf_on_device:
        n_matrices_to_store += 1
    avail_mem = mem_GB * 1e9
    if sparse_data is not None:
        avail_mem = avail_mem - sparse_data.nbytes
    itemsize = 4
    if results_in_double_precision:
        itemsize = 8
    # required memory = n_matrices_to_store * n_frames**2/binning_factor * n_bins * 4 + data_size
    bin_factor = ceil(n_matrices_to_store * n_frames * (n_frames + 1) * n_bins * itemsize / avail_mem)
    if suggest_friendly_binning_factor:
        while n_frames % bin_factor > 0:
            bin_factor += 1
    return bin_factor


# python counterpart of utils.cl:binning_function_log10
def binning_function_log10(dt, n_frames, endpoint=False, npts_per_decade=10):
    if dt == 0:
        return 0
    if endpoint:
        log_nframes = log10(n_frames)
        k_f = log10(dt) * (npts_per_decade * log_nframes - 1) / (log_nframes)
    else:
        k_f = log10(dt) * npts_per_decade
    return 1 + round(k_f)  # Add 1 because the [0] index is included on the leftmost side
