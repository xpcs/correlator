import numpy as np

from ..base import CorrelationResult
from ..opencl.correlator import OpenclCorrelator


class EventCorrelator(OpenclCorrelator):
    kernel_files = ["evtcorrelator.cl"]

    """
    A class to compute the correlation function for sparse XPCS data.
    """

    kernel_files = ["evtcorrelator.cl"]

    def __init__(
        self,
        n_frames,
        qmask,
        max_events_count,
        dtype=np.uint8,
        scale_factors=None,
        extra_options=None,
        **openclprocessing_kwargs,
    ):
        super().__init__(
            n_frames,
            qmask,
            dtype=dtype,
            scale_factors=scale_factors,
            extra_options=extra_options,
            **openclprocessing_kwargs,
        )
        self.max_events_count = max_events_count
        self._setup_kernels()

    def _setup_kernels(self):
        self._setup_and_compile_opencl_kernels(other_compilation_flags=["-DMAX_EVT_COUNT=%d" % self.max_events_count])
        self.correlation_kernel = self.kernels.get_kernel("event_correlator")
        self.normalization_kernel = self.kernels.get_kernel("normalize_correlation")
        self.grid = self.shape[::-1]
        self.wg = None  # tune ?

    def _prepare_results_arrays(self):
        self.get_opencl_array("d_res", self.output_shape, dtype=np.float32)
        self.get_opencl_array("d_res_int", self.output_shape, dtype=self._res_dtype)
        self.get_opencl_array("d_sums", self.output_shape, dtype=self._res_dtype)

        self.d_res.fill(0)
        self.d_sums.fill(0)

    def correlate(self, t_sparse_data, pre_sort_data=True):
        self._check_dtypes(t_sparse_data)
        d_t_sparse_data = self._data_to_cl_arrays(t_sparse_data)
        self._prepare_results_arrays()
        evt = self.correlation_kernel(
            self.queue,
            self.grid,
            self.wg,
            d_t_sparse_data.times.data,
            d_t_sparse_data.data.data,
            d_t_sparse_data.offsets.data,
            self.d_qmask.data,
            self.d_res_int.data,
            self.d_sums.data,
            np.int32(self.shape[1]),
            np.int32(self.shape[0]),
            np.int32(self.n_frames),
            np.int32(pre_sort_data),  # pre-sort
        )
        evt.wait()
        self.profile_add(evt, "Event correlator")

        evt = self.normalization_kernel(
            self.queue,
            (self.n_frames, self.n_bins),
            None,  # tune wg ?
            self.d_res_int.data,
            self.d_res.data,
            self.d_sums.data,
            self.d_scale_factors.data,
            np.int32(self.n_frames),
            np.int32(self.n_bins),
        )
        evt.wait()
        self.profile_add(evt, "Normalization")

        return CorrelationResult(correlation=self.d_res)
