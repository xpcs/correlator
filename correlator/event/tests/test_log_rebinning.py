import numpy as np
import pytest

from correlator.dense.matmul import py_dense_correlator
from correlator.event.event_matrix import (
    SMatrixEventCorrelator,
    TMatrixEventCorrelator,
    binning_function_log10,
    flat_to_square,
)
from correlator.event.tests.test_ttcf import check_ttcf_results, compare, datasets
from correlator.sparse.space import SpaceCompactedData, space_to_dense
from correlator.sparse.time_opencl import SpaceToTimeCompaction
from correlator.testutils import load_xpcs_compacted_data
from correlator.utils import binning as bin_array
from correlator.utils import tinker_ttcf_diagonal


def _get_space_compacted_data(dataset_name):
    dataset = datasets[dataset_name]
    data_fname = dataset["data_fname"]
    qmask_fname = dataset["qmask_fname"]
    n_frames = dataset["n_frames_for_test"]

    data, pix_idx, offset, qmask, frame_shape = load_xpcs_compacted_data(
        data_fname, qmask_fname, n_frames=n_frames, cast_to_dtype=np.uint8
    )
    s_sparse_data = SpaceCompactedData(data=data, indices=pix_idx, offsets=offset, frame_shape=frame_shape)

    return s_sparse_data, qmask


def ttcf_denominator(data, qmask, tinker_diagonal=False, scale_factor=None, return_partial_sums=False):
    sums = (data * qmask).sum(axis=(1, 2))
    denom = np.multiply.outer(sums, sums)
    if tinker_diagonal:
        tinker_ttcf_diagonal(denom, scale_factor=scale_factor)

    if return_partial_sums:
        return denom, sums
    return denom


def ttcf_numerator(xpcs_data, mask, tinker_diagonal=False, scale_factor=None):
    ind = np.where(mask > 0)  # unused pixels are 0
    xpcs_data = np.array(xpcs_data[:, ind[0], ind[1]], np.float32)  # (n_tau, n_pix)
    meanmatr = np.mean(xpcs_data, axis=1)  # xpcs_data.sum(axis=-1).sum(axis=-1)/n_pix
    ltimes, lenmatr = np.shape(xpcs_data)  # n_tau, n_pix
    meanmatr.shape = 1, ltimes

    num = np.dot(xpcs_data, xpcs_data.T)

    if tinker_diagonal:
        tinker_ttcf_diagonal(num, scale_factor=scale_factor)

    return num


def rebin_log_naive(arr, R, phi, dtype=np.uint64, progress=False):
    # (t0, t1) --> (t0/R, phi(t1-t0))

    tqdm = lambda x: x
    if progress:
        # ruff: noqa: SIM105
        try:
            from tqdm import tqdm
        except ImportError:
            pass

    ny, nx = arr.shape
    res = np.zeros((ny // R, phi(nx - 1) + 1), dtype=dtype)
    for t0 in tqdm(range(ny)):
        y = t0 // R
        for t1 in range(t0, nx):
            x = phi(t1 - t0)
            res[y, x] += arr[t0, t1]
    return res


def test_log_rebinning(dataset_name="dataset_0002", binning_x=None, correlator_type="time"):
    # Correlate data with event correlator
    s_sparse_data, qmask = _get_space_compacted_data(dataset_name)
    n_frames = s_sparse_data.offsets.size - 1

    # ruff: noqa: F841

    if correlator_type == "time":
        space_to_time = SpaceToTimeCompaction(s_sparse_data.frame_shape, dtype=np.uint8)
        d_t_sparse_data = space_to_time.space_compact_to_time_compact(s_sparse_data)
        correlator = TMatrixEventCorrelator(n_frames, qmask, binning=binning_x, horizontal_binning="log10")
        res = correlator.correlate(d_t_sparse_data)
    else:
        correlator = SMatrixEventCorrelator(n_frames, qmask, binning=binning_x, horizontal_binning="log10")
        res = correlator.correlate(s_sparse_data)

    # Validation
    data = space_to_dense(s_sparse_data, frame_shape=qmask.shape)
    phi = lambda dt: binning_function_log10(dt, n_frames, npts_per_decade=correlator._binning_pts_per_decade)

    # Reference denominator
    ttcf_denominator_ref, ttcf_partial_sums_ref = ttcf_denominator(data, qmask == 1, return_partial_sums=True)
    assert np.allclose(ttcf_partial_sums_ref, correlator.d_sums[0].get())

    # Reference log-binning for denominator
    denom_rebinned_log_ref = rebin_log_naive(ttcf_denominator_ref, correlator.binning, phi, progress=True)
    assert np.allclose(correlator.d_sums_corr_matrix[0].get(), denom_rebinned_log_ref)

    # Reference numerator
    ttcf_numerator_ref = ttcf_numerator(data, qmask == 1)
    # Reference log-binning  for numerator
    ttcf_numerator_log_ref = rebin_log_naive(
        ttcf_numerator_ref, correlator.binning, phi, progress=True, dtype=np.float64
    )
    assert np.allclose(correlator.d_corr_matrix[0].get(), ttcf_numerator_log_ref)
