import os

import numpy as np
import pytest
import yaml

from correlator.event.event_matrix import SMatrixEventCorrelator, TMatrixEventCorrelator, flat_to_square
from correlator.sparse.space import SpaceCompactedData
from correlator.sparse.time_opencl import SpaceToTimeCompaction
from correlator.testutils import load_xpcs_compacted_data
from correlator.utils import binning as bin_array

mae = lambda x: np.nanmax(np.abs(x))


class DatasetReader:
    """

    Class to read datasets from a main directory of datsets for tests

    """

    def __init__(self, main_directory):
        self.main_directory = main_directory

    def _read_dataset(self, dataset_name):
        dataset_path = os.path.join(self.main_directory, dataset_name)
        if not os.path.isdir(dataset_path):
            raise NotADirectoryError(f"{dataset_path} is not a directory")
        yaml_file_path = os.path.join(dataset_path, "info.yaml")
        if not os.path.exists(yaml_file_path):
            raise FileNotFoundError(f"{yaml_file_path} does not exist")
        with open(yaml_file_path) as yaml_file:
            dataset_info = yaml.safe_load(yaml_file)
            # No we need to change here that the original path is replaced by the
            # path to the files in the subdirectories of the new main directory
            for key in dataset_info:
                if key in ["data_fname", "qmask_fname", "raw_data_fname", "reference_fname"]:
                    subfolder = {
                        "data_fname": "data",
                        "qmask_fname": "qmask",
                        "raw_data_fname": "raw_data",
                        "reference_fname": "reference",
                    }[key]
                    filename = os.path.basename(dataset_info[key])
                    dataset_info[key] = os.path.join(self.main_directory, dataset_name, subfolder, filename)
            return dataset_info

    def __getitem__(self, dataset_name):
        return self._read_dataset(dataset_name)


# main_directory = "/data/projects/xpcs_repository/correlator_test_data"
# main_directory='/home/gitlab-runner/xpcs/'

# This is ugly way how we define here the path to the test data
main_directory = os.environ["XPCS_TESTDATA_DIRECTORY"]
datasets = DatasetReader(main_directory)


@pytest.fixture(scope="class")
def bootstrap_test_ttcf(request):
    cls = request.cls

    # cls.datasets_to_test = ["dataset01_scan0007"]  # OK 15'00
    # cls.datasets_to_test = ["dataset01_scan0008"]  # OK 06'40
    # cls.datasets_to_test = ["dataset01_scan0009"]  # OK, 3'20
    # cls.datasets_to_test = ["dataset01_scan0010"]  # OK, 2'20
    # cls.datasets_to_test = ["dataset01_scan0011"]  # OK, 06'00
    # cls.datasets_to_test = ["dataset01_scan0012"]  # 05'50
    # cls.datasets_to_test = ["dataset01_scan0013"]  # OK, 48s
    cls.datasets_to_test = ["dataset_0002"]  # OK, 38s
    # cls.datasets_to_test = ["dataset03"] # OK, 6'30
    # cls.datasets_to_test = ["dataset04"]  # OK, 33s
    # cls.datasets_to_test = ["dataset05"] # OK, 01'40

    cls.dtype = np.uint8


def compare(res, ref, method_name, tol=1e-4, verbose=True):
    if np.dtype(res.dtype).kind == "i":
        max_err = np.max(np.abs(res.astype(np.int64) - ref.astype(np.int64)))
    else:
        max_err = mae(res - ref)
    if verbose:
        print("Max error for %s is %.2e" % (method_name, max_err))
    assert max_err <= tol, "Something wrong with %s" % (method_name)


def check_ttcf_results(ttcf_result, num, denom, qmask, ref, tol):
    # Reference results were only calculated for first bin
    n_els = (qmask == 1).sum()
    compare(ttcf_result.correlation[0].get(), ref["g2"], "ttcf_space:g2", tol=tol["g2"])
    compare(ttcf_result.std[0].get(), ref["std"], "ttcf_space:std", tol=tol["std"])
    compare(
        num,
        np.triu(ref["ttcf_numerator"]),
        "ttcf_space:numerator",
        tol=0,
    )
    compare(
        denom / (1.0 * n_els),
        np.triu(ref["ttcf_denominator"]) * n_els,
        "ttcf_space:denominator",
        tol=tol["denom"],
    )


@pytest.mark.usefixtures("bootstrap_test_ttcf")
class TestTTCF:
    def _test_ttcf_space_compacted_data(self, s_sparse_data, qmask, ref, tol):
        n_frames = s_sparse_data.offsets.size - 1
        correlator_space = SMatrixEventCorrelator(n_frames, qmask)
        res_s = correlator_space.correlate(s_sparse_data, normalize_ttcf_on_device=True, return_numpy_arrays=False)

        num = flat_to_square(correlator_space.d_corr_matrix[0].get())
        denom = flat_to_square(correlator_space.d_sums_corr_matrix[0].get())
        check_ttcf_results(res_s, num, denom, qmask, ref, tol)

    def _test_ttcf_time_compacted_data(self, s_sparse_data, qmask, ref, tol):
        n_frames = s_sparse_data.offsets.size - 1

        space_to_time = SpaceToTimeCompaction(s_sparse_data.frame_shape, dtype=self.dtype)
        d_t_sparse_data = space_to_time.space_compact_to_time_compact(s_sparse_data)

        correlator_time = TMatrixEventCorrelator(n_frames, qmask)
        res_t = correlator_time.correlate(d_t_sparse_data, normalize_ttcf_on_device=True, return_numpy_arrays=False)

        num = flat_to_square(correlator_time.d_corr_matrix[0].get())
        denom = flat_to_square(correlator_time.d_sums_corr_matrix[0].get())
        check_ttcf_results(res_t, num, denom, qmask, ref, tol)

    def test_ttcf_compacted_data(self):
        for dataset_name in self.datasets_to_test:
            dataset = datasets[dataset_name]
            data_fname = dataset["data_fname"]
            qmask_fname = dataset["qmask_fname"]
            n_frames = dataset["n_frames_for_test"]

            data, pix_idx, offset, qmask, frame_shape = load_xpcs_compacted_data(
                data_fname, qmask_fname, n_frames=n_frames, cast_to_dtype=self.dtype
            )
            s_sparse_data = SpaceCompactedData(data=data, indices=pix_idx, offsets=offset, frame_shape=frame_shape)

            ref_fd = np.load(datasets[dataset_name]["reference_fname"])
            ref = {
                "g2": ref_fd["ttcf"],  # name is messed up!
                "std": ref_fd["std"],
                "ttcf_numerator": ref_fd["num"],
                "ttcf_denominator": ref_fd["denom"],
            }
            self._test_ttcf_space_compacted_data(s_sparse_data, qmask, ref, dataset["test_tolerance"])
            self._test_ttcf_time_compacted_data(s_sparse_data, qmask, ref, dataset["test_tolerance"])


def _extract_diags(arr):
    """
    Re-order a 2D array so that diag(arr, k) is column k of output
    """
    nr, nc = arr.shape
    res = np.zeros((nc, nc))
    for k in range(nr):
        res[:, k] = np.hstack([np.diag(arr, k=k), np.zeros(k)])
    return res


def check_rebinned_ttcf_results(ttcf_result, num, denom, qmask, ref, tol, correlator_type, binning=1):
    # Reference results were only calculated for first bin
    n_els = (qmask == 1).sum()
    compare(
        ttcf_result.correlation[0].get(), ref["g2"], "ttcf_%s:g2 binning=%d" % (correlator_type, binning), tol=tol["g2"]
    )
    if binning == 1:  # TODO still an issue with STD when using binning.
        compare(
            ttcf_result.std[0].get(), ref["std"], "ttcf_%s:std binning=%d" % (correlator_type, binning), tol=tol["std"]
        )

    ref_num = np.triu(ref["ttcf_numerator"])
    ref_denom = np.triu(ref["ttcf_denominator"]) * n_els
    # rebinned TTCF matrix has a different layout: (t1, t2) are stored at coordinates (t1/b, t1-t2)
    ref_num = _extract_diags(ref_num)
    ref_denom = _extract_diags(ref_denom)
    if binning > 1:
        ref_num = bin_array(ref_num, (binning, 1), out_dtype=ref_num.dtype, normalize=False)
        ref_denom = bin_array(ref_denom, (binning, 1), out_dtype=ref_denom.dtype, normalize=False)

    denom = denom / (1.0 * n_els)

    compare(
        num,
        ref_num,
        "ttcf_%s:numerator binning=%d" % (correlator_type, binning),
        tol=0,
    )
    compare(
        denom,
        ref_denom,
        "ttcf_%s:denominator binning=%d" % (correlator_type, binning),
        tol=tol["denom"] * (binning**0.5),
    )


@pytest.mark.usefixtures("bootstrap_test_ttcf")
class TestBinnedTTCF:
    def _get_space_compacted_data(self, dataset_name):
        dataset = datasets[dataset_name]
        data_fname = dataset["data_fname"]
        qmask_fname = dataset["qmask_fname"]
        n_frames = dataset["n_frames_for_test"]

        data, pix_idx, offset, qmask, frame_shape = load_xpcs_compacted_data(
            data_fname, qmask_fname, n_frames=n_frames, cast_to_dtype=self.dtype
        )
        s_sparse_data = SpaceCompactedData(data=data, indices=pix_idx, offsets=offset, frame_shape=frame_shape)

        return s_sparse_data, qmask

    def _test_ttcf_space_compacted_data(self, s_sparse_data, qmask, ref, tol, binning=4):
        n_frames = s_sparse_data.offsets.size - 1
        correlator_space = SMatrixEventCorrelator(n_frames, qmask, binning=binning)
        res_s = correlator_space.correlate(s_sparse_data, normalize_ttcf_on_device=True, return_numpy_arrays=False)
        num = correlator_space.d_corr_matrix[0].get()
        denom = correlator_space.d_sums_corr_matrix[0].get()
        check_rebinned_ttcf_results(res_s, num, denom, qmask, ref, tol, "space", binning=binning)

    def _test_ttcf_time_compacted_data(self, s_sparse_data, qmask, ref, tol, binning=4):
        n_frames = s_sparse_data.offsets.size - 1
        space_to_time = SpaceToTimeCompaction(s_sparse_data.frame_shape, dtype=self.dtype)
        d_t_sparse_data = space_to_time.space_compact_to_time_compact(s_sparse_data)

        correlator_time = TMatrixEventCorrelator(n_frames, qmask, binning=binning)

        res_t = correlator_time.correlate(d_t_sparse_data, normalize_ttcf_on_device=True, return_numpy_arrays=False)
        num = correlator_time.d_corr_matrix[0].get()
        denom = correlator_time.d_sums_corr_matrix[0].get()
        check_rebinned_ttcf_results(res_t, num, denom, qmask, ref, tol, "time", binning=binning)

    def test_ttcf_compacted_data(self):
        for dataset_name in self.datasets_to_test:
            dataset = datasets[dataset_name]
            s_sparse_data, qmask = self._get_space_compacted_data(dataset_name)

            ref_fd = np.load(datasets[dataset_name]["reference_fname"])
            ref = {
                "g2": ref_fd["ttcf"],  # name is messed up!
                "std": ref_fd["std"],
                "ttcf_numerator": ref_fd["num"],
                "ttcf_denominator": ref_fd["denom"],
            }
            for binning in [1, 2, 4]:
                self._test_ttcf_space_compacted_data(
                    s_sparse_data, qmask, ref, dataset["test_tolerance"], binning=binning
                )
                self._test_ttcf_time_compacted_data(
                    s_sparse_data, qmask, ref, dataset["test_tolerance"], binning=binning
                )

    def test_ttcf_log_rebinning(self):
        # WIP
        for dataset_name in self.datasets_to_test:
            # dataset = datasets[dataset_name]
            s_sparse_data, qmask = self._get_space_compacted_data(dataset_name)
            n_frames = s_sparse_data.offsets.size - 1

            for binning in [None, 1, 2, 4]:
                correlator_space = SMatrixEventCorrelator(n_frames, qmask, binning=binning, horizontal_binning="log10")
                correlator_space.correlate(s_sparse_data)
