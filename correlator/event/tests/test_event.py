import numpy as np
import pytest

from correlator.event.event import EventCorrelator
from correlator.sparse.time import dense_to_times
from correlator.testutils import XPCSDataset

mae = lambda x: np.nanmax(np.abs(x))

# see testutils
datasets_to_test = ["eiger_514_10k"]


@pytest.fixture(scope="class")
def bootstrap_test_event(request):
    cls = request.cls

    cls.dataset = XPCSDataset("andor_1024_3k")
    cls.dataset.data = cls.dataset.data.astype(np.uint8)
    cls.max_nnz = 330  # for andor_1024_10k
    cls.scale_factors = np.array([1025171])  # number of pixels actually used
    cls.shape = cls.dataset.dataset_desc.frame_shape
    cls.n_frames = cls.dataset.dataset_desc.nframes
    cls.ref = cls.dataset.result
    cls.data_compacted = dense_to_times(cls.dataset.data)
    cls.tol = 5e-3


@pytest.mark.usefixtures("bootstrap_test_event")
class TestEvent:
    def compare(self, res, method_name):
        errors = res[:, 1:] - self.ref
        errors_max = np.max(np.abs(errors[:, :-1]), axis=1)  # last point is wrong in ref...

        for bin_idx in range(errors_max.shape[0]):
            assert errors_max[bin_idx] < self.tol, "%s: something wrong in bin index %d" % (method_name, bin_idx)

    def test_event_correlator(self):
        # Init correlator
        event_correlator = EventCorrelator(
            self.n_frames,
            np.ones(self.dataset.data.shape[1:], dtype=np.int8),
            self.max_nnz,
            dtype=np.uint8,
            scale_factors=self.scale_factors,
        )
        # Correlate
        res = event_correlator.correlate(self.data_compacted, pre_sort_data=False)
        self.compare(res.correlation.get(), "OpenCL event correlator")
