from os import path

import numpy as np
import pyopencl.array as parray
from pyopencl.tools import dtype_to_ctype

from ..opencl.processing import OpenclProcessing
from ..opencl.utils import get_opencl_srcfile
from .time import TimeCompactedData


class SpaceToTimeCompactionV1(OpenclProcessing):
    kernel_files = ["sparse.cl"]

    def __init__(self, shape, max_time_nnz=250, dtype=np.uint8, offset_dtype=np.uint32, **oclprocessing_kwargs):
        super().__init__(**oclprocessing_kwargs)
        self.shape = shape
        self.dtype = dtype
        self.offset_dtype = offset_dtype
        self._setup_kernels(max_time_nnz=max_time_nnz)

    def _setup_kernels(self, max_time_nnz=250):
        self.max_time_nnz = max_time_nnz
        self.compile_kernels(
            kernel_files=self.kernel_files,
            compile_options=[
                "-DDTYPE=%s" % dtype_to_ctype(self.dtype),
                "-DOFFSET_DTYPE=%s" % dtype_to_ctype(self.offset_dtype),
                "-DMAX_EVT_COUNT=%d" % self.max_time_nnz,
                "-I%s" % path.dirname(get_opencl_srcfile("dtypes.h")),
            ],
        )
        self.space_compact_to_time_compact_kernel = self.kernels.get_kernel("space_compact_to_time_compact")
        self.space_compact_to_time_compact_stage2_kernel = self.kernels.get_kernel(
            "space_compact_to_time_compact_stage2_sort"
        )
        self._d_t_counter = parray.zeros(self.queue, np.prod(self.shape), np.uint32)
        self._d_t_data_tmp = parray.zeros(self.queue, self.max_time_nnz * np.prod(self.shape), self.dtype)
        self._d_t_times_tmp = parray.zeros(self.queue, self.max_time_nnz * np.prod(self.shape), np.uint32)

    def _space_compact_to_time_compact_stage1(self, data, pixel_indices, offsets, max_nnz_space, start_frame=0):
        self._d_t_data_tmp.fill(0)
        self._d_t_times_tmp.fill(0)

        n_frames = offsets.size - 1

        wg = None
        grid = (max_nnz_space, 1)  # round to something more friendly ?

        evt = self.space_compact_to_time_compact_kernel(
            self.queue,
            grid,
            wg,
            data.data,
            pixel_indices.data,
            offsets.data,
            self._d_t_data_tmp.data,
            self._d_t_times_tmp.data,
            self._d_t_counter.data,
            np.int32(self.shape[1]),
            np.int32(self.shape[0]),
            np.int32(n_frames),
            np.int32(start_frame),
        )
        evt.wait()
        self.profile_add(evt, "space->time stage 1")

    def _space_compact_to_time_compact_stage2(self):
        # Could be made on device directly. But parray.cumsum() takes some time to compile.
        t_counter = self._d_t_counter.get()
        offsets1 = np.cumsum(t_counter, dtype=t_counter.dtype)
        offsets = np.hstack([np.array([0], dtype=self.offset_dtype), offsets1])

        d_t_offsets = parray.to_device(self.queue, offsets)
        d_t_data = parray.zeros(self.queue, offsets[-1], self._d_t_data_tmp.dtype)
        d_t_times = parray.zeros(self.queue, d_t_data.size, np.uint32)

        wg = None
        grid = (np.prod(self.shape), 1)

        evt = self.space_compact_to_time_compact_stage2_kernel(
            self.queue,
            grid,
            wg,
            self._d_t_data_tmp.data,
            self._d_t_times_tmp.data,
            d_t_offsets.data,
            d_t_data.data,
            d_t_times.data,
            np.int32(np.prod(self.shape)),
        )
        evt.wait()
        self.profile_add(evt, "space->time stage 2")

        return d_t_data, d_t_times, d_t_offsets

    def space_compact_to_time_compact(self, s_sparse_data, start_frame=0):
        data = s_sparse_data.data
        pixel_indices = s_sparse_data.indices
        offsets = s_sparse_data.offsets

        max_nnz_space = np.diff(offsets).max()

        # Might be good to have a more clever memory management
        d_data = parray.to_device(self.queue, data.astype(self.dtype))
        d_pixel_indices = parray.to_device(self.queue, pixel_indices.astype(np.uint32))
        d_offsets = parray.to_device(self.queue, offsets.astype(self.offset_dtype))
        #

        self._space_compact_to_time_compact_stage1(
            d_data, d_pixel_indices, d_offsets, max_nnz_space, start_frame=start_frame
        )
        d_t_data, d_t_times, d_t_offsets = self._space_compact_to_time_compact_stage2()

        return TimeCompactedData(data=d_t_data, times=d_t_times, offsets=d_t_offsets, frame_shape=self.shape)


class SpaceToTimeCompactionV2(OpenclProcessing):
    """
    Alternate implementation requiring much less memory, but the output results are not sorted
    """

    kernel_files = ["sparse.cl"]

    def __init__(self, shape, dtype=np.uint8, offset_dtype=np.uint32, **oclprocessing_kwargs):
        super().__init__(**oclprocessing_kwargs)
        self.shape = shape
        self.dtype = dtype
        self.offset_dtype = offset_dtype
        self._setup_kernels()

    def _setup_kernels(self):
        kernel_files = list(map(get_opencl_srcfile, self.kernel_files))
        compile_options = [
            "-DDTYPE=%s" % dtype_to_ctype(self.dtype),
            "-DOFFSET_DTYPE=%s" % dtype_to_ctype(self.offset_dtype),
            "-DMAX_EVT_COUNT=%d" % 10,  # Not used here
            "-I%s" % path.dirname(get_opencl_srcfile("dtypes.h")),
        ]
        if np.iinfo(self.offset_dtype).bits == 64:
            compile_options.append("-DDOUBLE_COMPUTATIONS")
        self.compile_kernels(kernel_files=kernel_files, compile_options=compile_options)
        self.space_compact_to_time_compact_kernel = self.kernels.get_kernel("space_compact_to_time_compact_v2_stage1")
        self.space_compact_to_time_compact_stage2_kernel = self.kernels.get_kernel(
            "space_compact_to_time_compact_v2_stage2"
        )
        self._d_t_counter = parray.zeros(self.queue, np.prod(self.shape), np.uint32)

    def _space_compact_to_time_compact_stage1(self, pixel_indices, offsets, max_nnz_space, start_frame=0):
        n_frames = offsets.size - 1

        self._offsets = offsets  # DEBUG

        wg = None
        grid = (max_nnz_space, 1)  # round to something more friendly ?

        evt = self.space_compact_to_time_compact_kernel(
            self.queue,
            grid,
            wg,
            pixel_indices.data,
            offsets.data,
            self._d_t_counter.data,
            np.int32(self.shape[1]),
            np.int32(self.shape[0]),
            np.int32(n_frames),
        )
        evt.wait()
        self.profile_add(evt, "space->time stage 1")

    def _space_compact_to_time_compact_stage2(self, data, pixel_indices, offsets, n_frames, max_nnz_space):
        # Could be made on device directly. But parray.cumsum() takes some time to compile.
        t_counter = self._d_t_counter.get()
        offsets1 = np.cumsum(t_counter, dtype=self.offset_dtype)
        offsets1 = np.hstack([np.array([0], dtype=self.offset_dtype), offsets1])
        # return None
        self._d_t_counter.fill(0)

        d_t_offsets = parray.to_device(self.queue, offsets1)
        d_t_offsets2 = d_t_offsets.copy()
        d_t_data = parray.zeros(self.queue, offsets1[-1], self.dtype)
        d_t_times = parray.zeros(self.queue, d_t_data.size, np.uint32)

        wg = None
        grid = (max_nnz_space, 1)

        evt = self.space_compact_to_time_compact_stage2_kernel(
            self.queue,
            grid,
            wg,
            data.data,
            pixel_indices.data,
            offsets.data,
            d_t_data.data,
            d_t_times.data,
            d_t_offsets2.data,
            np.int32(n_frames),
        )
        evt.wait()
        self.profile_add(evt, "space->time stage 2")

        return d_t_data, d_t_times, d_t_offsets

    def space_compact_to_time_compact(self, s_sparse_data, start_frame=0):
        data = s_sparse_data.data
        pixel_indices = s_sparse_data.indices
        offsets = s_sparse_data.offsets

        max_nnz_space = np.diff(offsets).max()
        n_frames = offsets.size - 1

        # Might be good to have a more clever memory management
        d_data = parray.to_device(self.queue, data.astype(self.dtype))
        d_pixel_indices = parray.to_device(self.queue, pixel_indices.astype(np.uint32))
        d_offsets = parray.to_device(self.queue, offsets.astype(self.offset_dtype))
        #

        self._space_compact_to_time_compact_stage1(d_pixel_indices, d_offsets, max_nnz_space, start_frame=start_frame)
        d_t_data, d_t_times, d_t_offsets = self._space_compact_to_time_compact_stage2(
            d_data, d_pixel_indices, d_offsets, n_frames, max_nnz_space
        )

        return TimeCompactedData(data=d_t_data, times=d_t_times, offsets=d_t_offsets, frame_shape=self.shape)

    def get_timings(self):
        if not (self.profile):
            raise RuntimeError("Need to instantiate this class with profile=True")
        evd = lambda e: (e.stop - e.start) / 1e6
        return {e.name: evd(e) for e in self.events}


# Alias
SpaceToTimeCompaction = SpaceToTimeCompactionV2
