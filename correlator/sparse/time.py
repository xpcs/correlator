from dataclasses import dataclass
from math import ceil

import numpy as np


@dataclass
class TimeCompactedData:
    data: np.ndarray = None
    times: np.ndarray = None
    offsets: np.ndarray = None
    frame_shape: tuple = None

    @property
    def nbytes(self):
        return self.data.nbytes + self.times.nbytes + self.offsets.nbytes


def dense_to_times(frames):
    assert frames.ndim == 3
    framesT = np.moveaxis(frames, 0, -1)
    times = np.arange(frames.shape[0], dtype=np.uint32)
    nnz_indices = np.where(framesT > 0)
    frame_shape = frames.shape[1:]

    res_data = framesT[nnz_indices]
    res_times = times[nnz_indices[-1]]

    offsets = np.cumsum((frames > 0).sum(axis=0).ravel())
    res_offsets = np.zeros(np.prod(frames.shape[1:]) + 1, dtype=np.uint32)
    res_offsets[1:] = offsets[:]

    return TimeCompactedData(data=res_data, times=res_times, offsets=res_offsets, frame_shape=frame_shape)


def times_to_dense(t_sparse_data, n_frames=None, frame_shape=None):
    # infer missing parameters from sparse data
    if n_frames is None:
        n_frames = t_sparse_data.times[-1] + 1
    if frame_shape is None:
        frame_shape = TimeCompactedData.frame_shape
    if frame_shape is None:
        raise ValueError("Need to provide 'frame_shape'")

    res = np.zeros((n_frames,) + frame_shape, dtype=t_sparse_data.data.dtype)
    for i in range(frame_shape[0]):
        for j in range(frame_shape[1]):
            idx = i * frame_shape[1] + j
            start, stop = t_sparse_data.offsets[idx], t_sparse_data.offsets[idx + 1]
            if stop - start == 0:
                continue
            events = t_sparse_data.data[start:stop]
            times = t_sparse_data.times[start:stop]
            res[times, i, j] = events
    return res


def space_to_times(s_sparse_data, shape=None, max_nnz=None):
    data = s_sparse_data.data
    pix_idx = s_sparse_data.indices
    offset = s_sparse_data.offsets

    # infer missing parameters from sparse data
    if max_nnz is None:
        max_nnz = estimate_max_events_in_times_from_space_compacted_data(
            s_sparse_data, estimate_from_n_frames=offset.size - 1
        )
    if shape is None:
        shape = s_sparse_data.frame_shape
    if shape is None:
        raise ValueError("Need to provide 'shape'")

    t_data_tmp = np.zeros((max_nnz, np.prod(shape)), dtype=s_sparse_data.data.dtype)
    t_times_tmp = np.zeros(t_data_tmp.shape, dtype=np.uint32)
    t_counter = np.zeros(np.prod(shape), dtype=np.uint32)

    n_times = len(offset) - 1
    for i in range(n_times):
        indices = pix_idx[offset[i] : offset[i + 1]]
        z = t_counter[indices]
        t_data_tmp[z, indices] = data[offset[i] : offset[i + 1]]
        t_times_tmp[z, indices] = i
        t_counter[indices] += 1

    t_offsets = np.hstack([[0], np.cumsum(t_counter, dtype=np.uint32)])
    t_data = np.zeros(t_offsets[-1], dtype=data.dtype)
    t_times = np.zeros(t_data.size, dtype=np.uint32)
    for i in range(len(t_offsets) - 1):
        start, stop = t_offsets[i], t_offsets[i + 1]
        if start == stop:
            continue
        t_data[start:stop] = t_data_tmp[: stop - start, i]
        t_times[start:stop] = t_times_tmp[: stop - start, i]
    return TimeCompactedData(data=t_data, times=t_times, offsets=t_offsets, frame_shape=shape)


def estimate_max_events_in_times_from_space_compacted_data(s_sparse_data, estimate_from_n_frames=100):
    """
    From data in space-compacted format (data, pix_idx, offsets),
    return an estimation of the maximum number of events along the time axis,
    i.e (xpcs > 0).sum(axis=0).max().
    """
    num_events_in_subset = np.bincount(s_sparse_data.indices[: s_sparse_data.offsets[estimate_from_n_frames]]).max()
    return ceil((num_events_in_subset / estimate_from_n_frames) * (s_sparse_data.offsets.size - 1))
