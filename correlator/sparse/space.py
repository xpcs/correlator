from dataclasses import dataclass
from math import ceil
from multiprocessing.pool import ThreadPool

import numpy as np


@dataclass
class SpaceCompactedData:
    data: np.ndarray = None
    indices: np.ndarray = None
    offsets: np.ndarray = None
    frame_shape: tuple = None

    @property
    def nbytes(self):
        return self.data.nbytes + self.indices.nbytes + self.offsets.nbytes


# eg. dense_to_space(xpcs * (qmask > 0))
def dense_to_space(xpcs_frames):
    data = []  # int8
    pixel_idx = []  # uint32
    offset = [0]  # uint64
    frame_shape = xpcs_frames.shape[1:]  # first index is frame number

    for frame in xpcs_frames:
        frame_data = frame.ravel()
        nnz_idx = np.nonzero(frame_data)[0]
        data.append(frame_data[nnz_idx])
        pixel_idx.append(nnz_idx)
        offset.append(len(nnz_idx))
    return SpaceCompactedData(
        data=np.hstack(data), indices=np.hstack(pixel_idx), offsets=np.cumsum(offset), frame_shape=frame_shape
    )


def concatenate_space_compacted_data(*sparse_data):
    if not all([isinstance(s, SpaceCompactedData) for s in sparse_data]):
        raise TypeError("Expected list of SpaceCompactedData objects")
    res_data = np.concatenate([s.data for s in sparse_data])
    res_pix_idx = np.concatenate([s.indices for s in sparse_data])
    offsets = [s.offsets[1:].copy() for s in sparse_data]
    for i in range(1, len(offsets)):
        offsets[i] += offsets[i - 1][-1]
    res_offsets = np.concatenate(offsets)
    res_offsets = np.hstack([[0], res_offsets])
    return SpaceCompactedData(data=res_data, indices=res_pix_idx, offsets=res_offsets)


def dense_to_space_multi(xpcs_frames, n_threads=16):
    n_frames = xpcs_frames.shape[0]
    chunk_size = ceil(n_frames / n_threads)
    results = [None] * n_threads

    def compact_frames(i):
        results[i] = dense_to_space(xpcs_frames[i * (chunk_size) : (i + 1) * chunk_size])

    with ThreadPool(n_threads) as tp:
        tp.map(compact_frames, range(n_threads))

    return concatenate_space_compacted_data(*results)


def space_to_dense(sparse_data, frame_shape):
    n_frames = len(sparse_data.offsets) - 1
    xpcs_data = np.zeros((n_frames,) + frame_shape, dtype=sparse_data.data.dtype)

    def compact_to_frame(frame_idx):
        i_start = sparse_data.offsets[frame_idx]
        i_stop = sparse_data.offsets[frame_idx + 1]
        numels = np.prod(frame_shape)
        d = np.zeros(numels, dtype=sparse_data.data.dtype)
        pix_idx_ = sparse_data.indices[i_start:i_stop]
        data_ = sparse_data.data[i_start:i_stop]
        d[pix_idx_] = data_
        xpcs_data[frame_idx] = d.reshape(frame_shape)

    with ThreadPool(16) as tp:
        tp.map(compact_to_frame, range(n_frames))

    return xpcs_data
