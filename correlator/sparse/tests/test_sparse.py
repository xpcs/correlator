import os

import hdf5plugin
import numpy as np
import pytest
from silx.io import get_data
from silx.io.url import DataUrl

from correlator.opencl.utils import __has_pyopencl__
from correlator.sparse.space import dense_to_space, space_to_dense
from correlator.sparse.time import (
    TimeCompactedData,
    dense_to_times,
    estimate_max_events_in_times_from_space_compacted_data,
    space_to_times,
    times_to_dense,
)

if __has_pyopencl__:
    from correlator.sparse.time_opencl import SpaceToTimeCompactionV1, SpaceToTimeCompactionV2

# Fast patch to run test on dataset accesible for gitlab runner ,
#  to run pipline on Gitlab CI. To have flexibility in changing directory witht test data
# dataset_0003 is quiet heavy copied only to scisof10 raw_data (9GB)and qmask
xpcs_testdata_directory = os.environ["XPCS_TESTDATA_DIRECTORY"]
raw_data_path = os.path.join(xpcs_testdata_directory, "dataset_0003/raw_data/eiger4m_0000.h5")
qmask_path = os.path.join(xpcs_testdata_directory, "dataset_0003/qmask/Pt1_10GPa_2_242C_qmask.npy")

file_path = raw_data_path  # Naming not consisstent !


tests_config = {
    # TODO
    "xpcs_data_path": DataUrl(
        file_path=file_path,
        data_path="/entry_0000/measurement/data",
        data_slice=(slice(0, 100), slice(None, None), slice(None, None)),
        scheme="silx",
    ),
    "qmask_path": qmask_path,
    "data_type": np.uint8,
}


@pytest.fixture(scope="class")
def bootstrap(request):
    cls = request.cls
    cls.xpcs_frames = get_data(tests_config["xpcs_data_path"])
    cls.qmask = np.load(tests_config["qmask_path"])
    cls.xpcs_frames = (cls.xpcs_frames * (cls.qmask > 0)).astype(tests_config["data_type"])
    cls.n_frames = cls.xpcs_frames.shape[0]
    cls.frame_shape = cls.xpcs_frames.shape[1:]


@pytest.mark.usefixtures("bootstrap")
class TestSparseFormats:
    @staticmethod
    def compare_int_volumes(arr1, arr2, err_msg=""):
        # integer comparison - don't cast to float32, offsets int might be wrong after conversion
        abs_diff = np.abs(arr1.astype(np.int64) - arr2.astype(np.int64))
        assert abs_diff.max() == 0, err_msg

    def test_space_compaction(self):
        sparse_data = dense_to_space(self.xpcs_frames)
        # TODO compare with a reference ?
        dense = space_to_dense(sparse_data, self.frame_shape)
        self.compare_int_volumes(dense, self.xpcs_frames, "space compaction")

    def test_times_compaction(self):
        t_sparse_data = dense_to_times(self.xpcs_frames)
        # TODO compare with a reference ?
        dense = times_to_dense(t_sparse_data, n_frames=self.n_frames, frame_shape=self.frame_shape)
        self.compare_int_volumes(dense, self.xpcs_frames, "times compaction")

    def test_from_space_to_times(self):
        s_sparse_data = dense_to_space(self.xpcs_frames)
        t_sparse_data0 = dense_to_times(self.xpcs_frames)
        t_sparse_data = space_to_times(s_sparse_data, shape=self.frame_shape, max_nnz=20)
        self.compare_int_volumes(t_sparse_data.data, t_sparse_data0.data, "space -> times: data")
        self.compare_int_volumes(t_sparse_data.times, t_sparse_data0.times, "space -> times: times")
        self.compare_int_volumes(t_sparse_data.offsets, t_sparse_data0.offsets, "space -> times: offsets")

    @pytest.mark.skipif(not (__has_pyopencl__), reason="Need pyopencl for this test")
    def test_from_space_to_times_opencl(self):
        s_sparse_data = dense_to_space(self.xpcs_frames)
        t_sparse_data = dense_to_times(self.xpcs_frames)

        estimated_time_nnz = estimate_max_events_in_times_from_space_compacted_data(
            s_sparse_data, estimate_from_n_frames=self.n_frames
        )
        true_time_nnz = (self.xpcs_frames > 0).sum(axis=0).max()
        assert estimated_time_nnz == true_time_nnz

        estimated_time_nnz2 = estimate_max_events_in_times_from_space_compacted_data(
            s_sparse_data, estimate_from_n_frames=100
        )
        assert estimated_time_nnz2 >= true_time_nnz

        space_to_time_ocl = SpaceToTimeCompactionV1(
            self.frame_shape, max_time_nnz=estimated_time_nnz2, dtype=self.xpcs_frames.dtype
        )

        d_t_sparse_data = space_to_time_ocl.space_compact_to_time_compact(s_sparse_data)

        self.compare_int_volumes(d_t_sparse_data.data.get(), t_sparse_data.data)
        self.compare_int_volumes(d_t_sparse_data.times.get(), t_sparse_data.times)
        self.compare_int_volumes(d_t_sparse_data.offsets.get(), t_sparse_data.offsets)

    @pytest.mark.skipif(not (__has_pyopencl__), reason="Need pyopencl for this test")
    def test_from_space_to_times_opencl_v2(self):
        s_sparse_data = dense_to_space(self.xpcs_frames)

        space_to_time_ocl = SpaceToTimeCompactionV2(self.frame_shape, dtype=self.xpcs_frames.dtype)

        d_t_sparse_data = space_to_time_ocl.space_compact_to_time_compact(s_sparse_data)
        t_sparse_data = TimeCompactedData(
            data=d_t_sparse_data.data.get(), times=d_t_sparse_data.times.get(), offsets=d_t_sparse_data.offsets.get()
        )

        # SpaceToTimeCompactionV2 does not sort the results. It does not matter when computing the correlation function,
        # but the results cannot be compared directly to the output of dense_to_times().
        # Instead we make sure that the re-densified data is valid.
        data_dense = times_to_dense(
            t_sparse_data,
            n_frames=self.n_frames,
            frame_shape=self.frame_shape,
        )
        self.compare_int_volumes(self.xpcs_frames, data_dense)
