from dataclasses import dataclass

import numpy as np


@dataclass
class CorrelationResult:
    correlation: np.ndarray = None
    ttcf: np.ndarray = None
    std: np.ndarray = None


class BaseCorrelator:
    "Abstract base class for all Correlators"

    default_extra_options = {
        # dtype for indices offsets
        # must be extended to unsigned long (np.uint64) if total_nnz > 4294967295
        "offset_dtype": np.uint32,
        # dtype for intermediate result (correlation in integer).
        # must be extended to unsigned long for large nnz_per_frame and/or events counts
        "res_dtype": np.uint32,
        "sums_dtype": np.uint32,
        # dtype for q-mask.
        #  must be extended to int if number of q-bins > 127
        "qmask_dtype": np.int8,
    }

    def __init__(self, n_frames, qmask, dtype=np.uint8, scale_factors=None, extra_options=None):
        """
        Initialize a correlator

        Parameters
        -----------
        n_frames: int
            Number of frames in the dataset
        qmask: numpy.ndarray, optional
            Mask defining the region of each scattering vector.
            Pixels set to zero means that the values at these location won't be used at all (eg. defective detector pixel)
            If set to None, all pixels are used to the same scattering vector.
        dtype: str, or numpy.dtype, optional
            Data type for XPCS frames. Default is uint8 (unsigned char).
        scale_factors: float or numpy.ndarray, optional
            Value used for spatial averaging.
            If all the pixels in the frames are used for the correlation (i.e qmask is ones everywhere, or qmask is None),
            the spatial averaging is sum(frame)/frame.size.
            Otherwise, sum(frame) is divided by the number of pixels falling into the current bin (i.e len(qmask == bin)).
            You can speficy a different value: either a float (in this case the same normalization is used for all bins),
            or an array which must have the same length as the number of bins.


        Other parameters
        -----------------
        extra_options: dict, optional
            Advanced extra options. Available options are:
                - "offset_dtype": np.uint32
                - "res_dtype": np.uint32,
                - "sums_dtype": np.uint32,
                - "qmask_dtype": np.int8,
        """
        self.shape = qmask.shape
        self.n_frames = n_frames

        self._configure_extra_options(extra_options)
        self._set_dtype(dtype)
        self._set_qmask(qmask)
        self._set_scale_factors(scale_factors)
        self._init_finalize()

    def _init_finalize(self):
        pass

    def _set_dtype(self, dtype):
        # Configure data types - important for OpenCL kernels,
        # as some operations are better performed on integer types (ex. atomic),
        # but some overflow can occur for large/non-sparse data.

        # data type for data. Usually the data values lie in a small range (less than 255)
        self.dtype = dtype
        # Other data types
        self._offset_dtype = self.extra_options["offset_dtype"]
        self._qmask_dtype = self.extra_options["qmask_dtype"]
        self._res_dtype = self.extra_options["res_dtype"]
        self._sums_dtype = self.extra_options["sums_dtype"]
        self._pix_idx_dtype = np.uint32  # won't change (goes from 0 to N_x*N_y)
        self._output_dtype = np.float32  # won't change

    def _set_qmask(self, qmask):
        self.qmask = None
        if qmask is None:
            self.bins = np.array([1], dtype=self._qmask_dtype)
            self.n_bins = 1
            self.qmask = np.ones(self.shape, dtype=self._qmask_dtype)
        else:
            self.qmask = np.ascontiguousarray(qmask, dtype=self._qmask_dtype)
            self.n_bins = self.qmask.max()
            self.bins = np.arange(1, self.n_bins + 1, dtype=self._qmask_dtype)
        self.output_shape = (self.n_bins, self.n_frames)

    def _set_scale_factors(self, scale_factor):
        if self.n_bins == 0:
            s = scale_factor or np.prod(self.shape)
            self.scale_factors = {1: s}
            return
        if scale_factor is not None:
            if not (np.iterable(scale_factor)):
                scale_factor = [scale_factor]
            assert len(scale_factor) == self.n_bins
            if isinstance(scale_factor, dict):
                self.scale_factors = scale_factor
            else:
                self.scale_factors = {k: v for k, v in zip(self.bins, scale_factor)}
        else:
            self.scale_factors = {}
            for bin_val in self.bins:
                self.scale_factors[bin_val] = np.sum(self.qmask == bin_val)

    def _configure_extra_options(self, extra_options):
        """
        :param extra_options: dict
        """
        self.extra_options = self.default_extra_options.copy()
        self.extra_options.update(extra_options or {})

    def get_correlation_result(self, calc_std=False, calc_ttcf=False):
        """
        Pre-allocate arrays and return a CorrelationResult object
        """
        g2 = np.zeros((self.n_bins, self.n_frames), dtype=np.float32)
        if calc_std:
            std = np.zeros_like(g2)
        else:
            std = None
        if calc_ttcf:
            ttcf = np.zeros((self.n_bins, self.n_frames, self.n_frames), dtype=np.float32)
        else:
            ttcf = None
        return CorrelationResult(correlation=g2, std=std, ttcf=ttcf)
