import os

version = "2024.1.0-dev"

# Set the environment variable for the test data directory - ugly way to do it
if "XPCS_TESTDATA_DIRECTORY" not in os.environ:
    os.environ["XPCS_TESTDATA_DIRECTORY"] = "/data/projects/xpcs_repository/correlator_test_data"
