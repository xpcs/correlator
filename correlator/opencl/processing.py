import os

import numpy as np
import pyopencl.array as parray
from silx.opencl.common import pyopencl as cl
from silx.opencl.processing import KernelContainer
from silx.opencl.processing import OpenclProcessing as SilxOpenclProcessing

from .utils import get_opencl_srcfile


class OpenclProcessing(SilxOpenclProcessing):
    """
    OpenclProcessing from silx, plus extra features
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._allocated = {}

    #
    # Memory management
    #

    def get_opencl_array(self, array_name, shape, dtype=np.float32):
        """
        Allocate a GPU array on the current context/stream/device if needed
        and set 'self.array_name' to this array.
        If an array with the same name already exists, return this array.

        Parameters
        ----------
        array_name: str
            Name of the array (for book-keeping)
        shape: tuple of int
            Array shape
        dtype: numpy.dtype, optional
            Data type. Default is float32.
        """
        if not self._allocated.get(array_name, False):
            new_device_arr = parray.zeros(self.queue, shape, dtype)
            setattr(self, array_name, new_device_arr)
            self._allocated[array_name] = True
        return getattr(self, array_name)  # TODO check shape! it'll fail if new shape is different

    def set_array(self, array_name, array_ref):
        """
        Set the content of a device array.

        Parameters
        ----------
        array_name: str
            Array name. This method will look for self.array_name.
        array_ref: array (numpy or GPU array)
            Array containing the data to copy to 'array_name'.
        """
        if isinstance(array_ref, parray.Array):
            current_arr = getattr(self, array_name, None)
            setattr(self, "_old_" + array_name, current_arr)
            setattr(self, array_name, array_ref)
        elif isinstance(array_ref, np.ndarray):
            self.get_opencl_array(array_name, array_ref.shape, dtype=array_ref.dtype)
            getattr(self, array_name).set(array_ref)
        else:
            raise ValueError("Expected numpy array or pyopencl array")
        return getattr(self, array_name)

    def get_array(self, array_name):
        return getattr(self, array_name, None)

    #
    # Patches to base class
    #

    def compile_kernels(self, kernel_files=None, compile_options=None):
        # Overwrite OpenclProcessing.compile_kernel, as it does not support
        # kernels outside silx/opencl/resources
        kernel_files = kernel_files or self.kernel_files
        kernel_files = [get_opencl_srcfile(kern_file) for kern_file in kernel_files]

        allkernels_src = []
        for kernel_file in kernel_files:
            with open(kernel_file) as fid:
                kernel_src = fid.read()
            allkernels_src.append(kernel_src)
        allkernels_src = os.linesep.join(allkernels_src)

        compile_options = compile_options or self.get_compiler_options()
        try:
            self.program = cl.Program(self.ctx, allkernels_src).build(options=compile_options)
        except (cl.MemoryError, cl.LogicError) as error:
            raise MemoryError(error)
        else:
            self.kernels = KernelContainer(self.program)

    #
    # Extra utils
    #

    def get_timings(self):
        if not (self.profile):
            raise RuntimeError("Need to instantiate this class with profile=True")
        evd = lambda e: (e.stop - e.start) / 1e6
        return {e.name: evd(e) for e in self.events}
