import os

from ..utils import get_folder_path

try:
    import pyopencl

    __has_pyopencl__ = True
except:
    __has_pyopencl__ = False


def get_opencl_srcfile(filename):
    src_relpath = os.path.join("opencl", "src")
    opencl_src_folder = get_folder_path(foldername=src_relpath)
    return os.path.join(opencl_src_folder, filename)
