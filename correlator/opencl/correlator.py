from os import path

import numpy as np
import pyopencl.array as parray
from pyopencl.tools import dtype_to_ctype

from ..base import BaseCorrelator
from ..sparse.space import SpaceCompactedData
from ..sparse.time import TimeCompactedData
from ..utils import docstring
from .processing import OpenclProcessing
from .utils import get_opencl_srcfile


class OpenclCorrelator(BaseCorrelator, OpenclProcessing):
    @docstring(BaseCorrelator)
    def __init__(
        self, n_frames, qmask, dtype=np.uint8, scale_factors=None, extra_options=None, **openclprocessing_kwargs
    ):
        OpenclProcessing.__init__(self, **openclprocessing_kwargs)
        BaseCorrelator.__init__(
            self, n_frames, qmask, dtype=dtype, scale_factors=scale_factors, extra_options=extra_options
        )
        self._set_cl_dtypes()
        self.is_cpu = self.device.type == "CPU"
        self._set_cl_qmask()
        self._init_finalize_cl()

    def _init_finalize_cl(self):
        pass

    def _set_cl_dtypes(self):
        self.c_dtype = dtype_to_ctype(self.dtype)
        self.c_sums_dtype = dtype_to_ctype(self._sums_dtype)
        self.idx_c_dtype = "int"  # TODO custom ?
        self._res_c_dtype = dtype_to_ctype(self._res_dtype)
        self._dtype_compilation_flags = [
            "-DDTYPE=%s" % self.c_dtype,
            "-DOFFSET_DTYPE=%s" % (dtype_to_ctype(self._offset_dtype)),
            "-DQMASK_DTYPE=%s" % (dtype_to_ctype(self._qmask_dtype)),
            "-DRES_DTYPE=%s" % self._res_c_dtype,
            "-I%s" % path.dirname(get_opencl_srcfile("dtypes.h")),
        ]
        if np.iinfo(self._res_dtype).bits == 64:
            self._dtype_compilation_flags.append("-DDOUBLE_COMPUTATIONS")

    def _set_cl_qmask(self):
        self.set_array("d_qmask", self.qmask)
        self.set_array("d_scale_factors", np.array(list(self.scale_factors.values()), dtype="f"))

    def _setup_and_compile_opencl_kernels(self, other_compilation_flags=None):
        compilation_flags = self._dtype_compilation_flags.copy()
        if other_compilation_flags is not None:
            compilation_flags += other_compilation_flags
            self._last_compilation_flags = compilation_flags.copy()
        self.compile_kernels(compile_options=compilation_flags)
        # self.kernels

    def _data_to_cl_arrays(self, sparse_data):
        """
        For each of the arrays in "sparse_data":
          - If the array is already a device (OpenCL) array: use this array reference
          - Otherwise (numpy array):
            - If self._d_this_array is not already allocated: allocate it
            - Transfer the content of numpy array to device array
        """
        what = {"data": sparse_data.data, "offsets": sparse_data.offsets}
        if isinstance(sparse_data, SpaceCompactedData):
            what["indices"] = sparse_data.indices
            cls = SpaceCompactedData
        elif isinstance(sparse_data, TimeCompactedData):
            what["times"] = sparse_data.times
            cls = TimeCompactedData
        else:
            raise TypeError("Expected 'sparse_data' to be a SpaceCompactedData or TimeCompactedData object")
        new_arrays_refs = {}
        for arr_name, arr in what.items():
            if isinstance(arr, parray.Array):
                new_arrays_refs[arr_name] = arr
            if isinstance(arr, np.ndarray):
                d_arr = self.get_opencl_array("d_" + arr_name, arr.shape, arr.dtype)
                d_arr[:] = arr[:]  # copy H2D
                new_arrays_refs[arr_name] = d_arr
        return cls(**new_arrays_refs)

    def _check_dtypes(self, sparse_data):
        dtypes = [
            ("data", self.dtype, sparse_data.data),
            ("offsets", self._offset_dtype, sparse_data.offsets),
        ]
        if isinstance(sparse_data, SpaceCompactedData):
            dtypes.append(("indices", np.uint32, sparse_data.indices))
        elif isinstance(sparse_data, TimeCompactedData):
            dtypes.append(("times", np.uint32, sparse_data.times))
        else:
            raise TypeError("Expected either SpaceCompactedData or TimeCompactedData object")
        for arr_name, dtype, arr in dtypes:
            if arr.dtype != dtype:
                raise ValueError("Expected dtype=%s for '%s', but got dtype=%s" % (dtype, arr_name, arr.dtype))
