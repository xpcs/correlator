#ifndef UTILS_H
#define UTILS_H

static inline size_t flattened_matrix_get_index(uint W, uint x, uint y);
static inline size_t flattened_matrix_get_index_3D(uint W, uint x, uint y, uint qbin_idx);
static inline size_t binning_function(uint x);
static inline size_t binned_matrix_get_index(uint W0, uint x0, uint y0, uint binning_factor);
static inline size_t binned_matrix_get_index_3D(uint n_frames, uint W0, uint x0, uint y0, uint qbin_idx, uint binning_factor);
static inline uint binary_search(uint val, uint* arr, uint n);
kernel void build_scalar_correlation_matrix(
    const global RES_DTYPE* arr,
    global RES_DTYPE* res,
    int n_frames,
    int horiz_width,
    #ifdef DO_BINNING
    int binning_factor,
    #endif
    int n_bins
);

#endif
