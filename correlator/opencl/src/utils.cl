#ifdef DOUBLE_COMPUTATIONS
  #pragma OPENCL EXTENSION cl_khr_int64_base_atomics : enable
#endif


/*
  From the index (x, y) in the 2D correlation matrix,
  get the index in the flattened correlation array.

  The output correlation matrix is flattened, and only the upper part is retained.
  So instead of having W*W elements (where W is the matrix width), [W | W | ... | W]
  We have W * (W+1)//2 elements  [W | W - 1 | W - 2 | ... 1 ]
  Element at index (x, y) in the 2D matrix corresponds to index   W * y - y*(y-1)//2 + x - y  in the flattened array

  W: matrix width
  x: column index
  y : row index
*/
static inline size_t flattened_matrix_get_index(uint W0, uint x0, uint y0) {
    // return (size_t) (W * y) - (size_t) (y*(y-1)/2) + x - y;
    size_t W = (size_t) W0;
    size_t x = (size_t) x0;
    size_t y = (size_t) y0;
    return (size_t) (W * y) - (size_t) (y*(y-1)/2) + x - y;
}


/*
  Same as flattened_matrix_get_index(), but extended for a stack of correlation matrices.
  correlation_matrices[qbin, y, x]   -->   flattened_matrices[get_index_3D(qbin, y, x))]
*/
static inline size_t flattened_matrix_get_index_3D(uint W0, uint x0, uint y0, uint qbin_idx) {
    size_t out_idx = flattened_matrix_get_index(W0, x0, y0);
    out_idx += qbin_idx * ((W0 * (W0 + 1)) / 2);
    return out_idx;
}


#ifndef BINNING_PTS_PER_DECADE
  #define BINNING_PTS_PER_DECADE 10
#endif

#define BINNING_FCT_IDENTITY 0
#define BINNING_FCT_LOG 1
#define BINNING_FCT_LOG2 2
#define BINNING_FCT_LOG10 3
#define BINNING_FCT_ARCSINH 4


/**
Returns the index, in 1D array indexed in logarithmic scale, corresponding to a given value.

If "N" denotes the number of frames, using a logarithmic scale with "d" points per decade:
  logspace(0, log10(N), d*log10(N))  ==  10^linspace(0, log10(N), d*log10(N))
where
  linspace(a, b, n)[k] = a + k*(b - a)/(n-delta)   where delta = 1 if endpoint=True, 0 otherwise.
therefore, t1-t2 on a logarithmic scale corresponds to
  t1 - t2 = 10^(a + k*(b-a)/(n-delta))
which corresponds to array index

  k = (log10(t1-t2) - a) * (d * log10(N) - delta) / (log10(N) - a)

where
  * "a = 0" is the starting point for logspace()
  * d is the number of points per decade (10-20)
  * delta = 1 if endpoint=True, 0 otherwise

Example: log scale up to 100, with 5 points per decade:
  np.logspace(0, np.log10(1e2), int(5*np.log10(1e2))
  [1.0, 1.67, 2.78, 4.64, 7.74, 12.92, 21.54, 35.94, 59.95, 100.0]
   t1-t2 = 19 corresponds to index k = 5.75   :  log10(40)*(5*2-1)/2
   t1-t2 = 40 corresponds to index k = 7.2

Note that an offset of 1 is added since this log-space also accounts for the [0] position.
**/
static inline size_t binning_function_log10(uint dt) {
    if (dt == 0) return 0;
    // float k_f = log10((float) dt) * (BINNING_PTS_PER_DECADE * LOG_N_FRAMES - 1) / (LOG_N_FRAMES); // endpoint=True
    float k_f = log10((float) dt) * BINNING_PTS_PER_DECADE; // endpoint=False
    return (size_t) (1 + round(k_f)); // Add 1 because the [0] index is included on the leftmost side
}

static inline size_t binning_function(uint x) {
    // log(), arcsinh() and so on can only be computed on float32 or float64.
    // In principle simple precision (float32) should not be used, as uint integers are not exactly represented above 2**24.
    // However in practice n_frames should be less than 16 million, and even so, the function is applied right after.
    // The maximum error will be phi(2**(n-24)) if x >= 2**n
    #if BINNING_FUNC == BINNING_FCT_LOG10
    return binning_function_log10(x);
    #else
    return (size_t) x;
    #endif
}



// (x, y) --> (x//binning, phi(x-y))
static inline size_t binned_matrix_get_index(uint W0, uint x0, uint y0, uint binning_factor) {
    // TODO check that x >= y, as we are doing arithmetic with uint
    #ifdef BINNING_FUNC
    return (y0 / binning_factor) * W0 + binning_function(x0 - y0);
    #else
    return (y0 / binning_factor) * W0 + (x0 - y0);
    #endif
}


static inline size_t binned_matrix_get_index_3D(uint n_frames, uint W0, uint x0, uint y0, uint qbin_idx, uint binning_factor) {
    size_t out_idx = binned_matrix_get_index(W0, x0, y0, binning_factor);
    out_idx += qbin_idx * (n_frames / binning_factor) * W0;
    return out_idx;
}


/*
  Binary search in an array of (unsigned) integers.

  Parameters
  -----------
  val: query element
  arr: (sorted) array
  n: array size

  Returns
  -------
  idx: Location of found element. If no element is found, return the array size.
*/
static inline uint binary_search(uint val, uint* arr, uint n) {
    uint L = 0, R = n - 1;
    uint m = 0;
    while (L != R) {
        m = (L + R + 1)/2;
        if (arr[m] > val) R = m - 1;
        else L = m;
    }
    if (arr[L] == val) return L;
    return n;
}

// launched with (nframes, nframes, qbins) grid
// res[y * n + x] = arr[x] * arr[y];
kernel void build_scalar_correlation_matrix(
    const global RES_DTYPE* arr,
    global RES_DTYPE* res,
    int n_frames,
    int horiz_width,
    #ifdef DO_BINNING
    int binning_factor,
    #endif
    int n_bins
) {
    uint x = get_global_id(0);
    uint y = get_global_id(1);
    uint qbin_idx = get_global_id(2);

    uint x0 = x + n_frames * qbin_idx;
    uint y0 = y + n_frames * qbin_idx;

    if ((x >= n_frames) || (qbin_idx >= n_bins) || (y > x)) return;

    #ifdef DO_BINNING
    size_t idx = binned_matrix_get_index_3D(n_frames, horiz_width, x, y, qbin_idx, binning_factor);
    atomic_add(res + idx, arr[x0] * arr[y0]);
    #else
    if (y > x) return;
    size_t idx = flattened_matrix_get_index_3D(n_frames, x, y, qbin_idx);
    res[idx] = arr[x0] * arr[y0];
    #endif

}


static inline double2 update_variance(double2 avg_m2, int i, double newval) {
    double avg = avg_m2.s0, m2 = avg_m2.s1;
    double dev1 = newval - avg;
    avg = (i*avg + newval)/(1+i);
    double dev2 = newval - avg;
    m2 += dev1 * dev2;
    return (double2) (avg, m2);
}


/**
Launched with (n_frames, n_bins) threads.

Both numerator and denominator are stored as flattened matrix:
  [ W items | W-1 items | W-2 items | ... | 1 item]    W = n_frames

The aim is to compute
  - sum(diag_num)
  - sum(diag_denom)
  - variance(diag_num / diag_denom)


**/
kernel void get_g2_and_std_v1(
    const global RES_DTYPE* numerator,
    const global RES_DTYPE* denominator,
    const global float* scale_factors,
    global double* g2,
    global double* std,
    int n_frames,
    #ifdef DO_BINNING
    int binning_factor,
    #endif
    int n_bins
)
{
    uint tid = get_global_id(0);
    uint qbin_idx = get_global_id(1);
    if (qbin_idx >= n_bins || tid >= n_frames) return;
    uint temporal_scale_factor = (n_frames - tid);

    #ifdef DO_BINNING
    // binned matrix access index
    uint offset_for_qbin = (n_frames * (n_frames / binning_factor)) * qbin_idx;
    temporal_scale_factor /= binning_factor;
    #else
    // flattened matrix access index
    uint offset_for_qbin = ((n_frames * (n_frames + 1)) / 2) * qbin_idx;
    #endif

    double sum_diag_num = 0, sum_diag_denom = 0;
    double2 avg_m2 = (0, 0);

    uint offset = 0;
        #ifdef DO_BINNING
    // for (uint i = 0; i < (n_frames - tid) / binning_factor; i++) {
    for (uint i = 0; i < (n_frames) / binning_factor; i++) {
        #else
    for (uint i = 0; i < n_frames; i++) {
        if (tid >= n_frames - i) break;
        #endif
        RES_DTYPE n = numerator[offset_for_qbin + offset + tid];
        RES_DTYPE d = denominator[offset_for_qbin + offset + tid];

        sum_diag_num += (double) n;
        sum_diag_denom += (double) d;
        if (d > 0) avg_m2 = update_variance(avg_m2, i, n/ ((double) d));

        #ifdef DO_BINNING
        offset += n_frames;
        #else
        offset += n_frames - i;
        #endif

    }
    g2[qbin_idx * n_frames + tid] = sum_diag_num / sum_diag_denom * scale_factors[qbin_idx];
    std[qbin_idx * n_frames + tid] = sqrt(avg_m2.s1) / temporal_scale_factor * scale_factors[qbin_idx];
}

