#include "dtypes.h"
#ifdef DOUBLE_COMPUTATIONS
  #pragma OPENCL EXTENSION cl_khr_int64_base_atomics : enable
#endif
// Launched with (image_width, image_height) grid of threads.
// One thread handle one line of events, so threads are indexes by frame pixel indices.
kernel void event_correlator(
    const global uint* vol_times_array,
    const global DTYPE* vol_data_array,
    const global OFFSET_DTYPE* offsets,
    const global QMASK_DTYPE* q_mask,
    global RES_DTYPE* res_tau,
    global RES_DTYPE* sums,
    int image_width,
    int image_height,
    int Nt,
    int pre_sort
) {
    uint x = get_global_id(0);
    uint y = get_global_id(1);
    if ((x >= image_width) || (y >= image_height)) return;
    uint pos = y*image_width + x;

    int bin_idx = q_mask[pos] - 1;
    if (bin_idx < 0) return;

    OFFSET_DTYPE offset = offsets[pos];
    uint n_events = offsets[pos+1] - offset;
    if (n_events == 0) return;

    global uint* vol_times = vol_times_array + offset;
    global DTYPE* vol_data = vol_data_array + offset;

    // Fetch vol_XXX[y, x, :] in fast memory
    uint times[MAX_EVT_COUNT] = {0};
    DTYPE data[MAX_EVT_COUNT] = {0};
    for (uint i = 0; i < n_events; i++) {
        times[i] = vol_times[i];
        data[i] = vol_data[i];
    }


    // If time-compacted data is unordered, it has to be sorted here
    if (pre_sort) {
        uint i = 1, j;
        while (i < n_events) {
            j = i;
            while (j > 0 && times[j-1] > times[j]) {
                uint tmp = times[j];
                times[j] = times[j-1];
                times[j-1] = tmp;

                DTYPE tmp2 = data[j];
                data[j] = data[j-1];
                data[j-1] = tmp2;
                j--;
            }
            i++;
        }
    }
    // ---


    // Compute the correlatin and store the result in the current bin
    global RES_DTYPE* my_res_tau = res_tau + bin_idx * Nt;
    global RES_DTYPE* my_sums = sums + bin_idx * Nt;
    for (uint i_tau = 0; i_tau < n_events; i_tau++) {
        atomic_add(my_sums + times[i_tau], data[i_tau]);
        for (uint i_t = i_tau; i_t < n_events; i_t++) {
            uint tau = times[i_t] - times[i_t - i_tau];
            atomic_add(my_res_tau + tau, data[i_t] * data[i_t - i_tau]);
        }
    }
}


// Normalize < <I(t, p) * I(t-tau, p)>_p >_t
// by  <I(t, p)>_p * <I(t-tau, p)>_p >_t
// i.e res_int is divided by correlate(sums, sums, "full")
// The kernel is launched with a grid size (n_frames, n_bins)
kernel void normalize_correlation(
    global RES_DTYPE* res_int,
    global float* res,
    global RES_DTYPE* sums,
    const global float* scale_factors,
    int Nt,
    int n_bins
) {
    uint tau = get_global_id(0);
    uint bin_idx = get_global_id(1);
    if ((tau >= Nt) || (bin_idx >= n_bins)) return;

    global RES_DTYPE* my_sum = sums + bin_idx * Nt;
    float corr = 0.0f;
    for (int t = tau; t < Nt; t++) {
        corr += (my_sum[t] * 1.0f * my_sum[t - tau]);
    }
    corr /= scale_factors[bin_idx]; // passing 1/scale_factor in preprocessor is not numerically accurate
    res[bin_idx * Nt + tau] = res_int[bin_idx*Nt + tau] / corr;
}

