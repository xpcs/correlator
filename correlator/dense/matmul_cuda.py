import numpy as np

from ..utils import updiv
from .matmul import MatMulCorrelator

try:
    import pycuda.gpuarray as garray
    from pycuda.compiler import SourceModule
except ImportError:
    CUFFT = None
try:
    import skcuda.linalg as cublas
    import skcuda.misc as skmisc
except ImportError:
    cublas = None


class CublasMatMulCorrelator(MatMulCorrelator):
    """
    The CublasMatMulCorrelator is a CUDA-accelerated version of MatMulCorrelator.

    Extra options
    --------------
    cublas_handle: int
        If provided, use this cublas handle instead of creating a new one.
    """

    def _init_finalize(self):
        if cublas is None:
            raise ImportError("scikit-cuda is needed to use this correlator")
        self._init_cublas()
        self._compile_kernels()

    def _init_cublas(self):
        import pycuda.autoinit

        if "cublas_handle" in self.extra_options:
            handle = self.extra_options["cublas_handle"]
        else:
            handle = skmisc._global_cublas_handle
            if handle is None:
                cublas.init()  # cublas handle + allocator
                handle = skmisc._global_cublas_handle
        self.cublas_handle = handle

    def _compile_kernels(self):
        mod = SourceModule(
            """
            // Extract the upper diagonals of a square (N, N) matrix.
            __global__ void extract_upper_diags(float* matrix, float* diags, int N) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if ((x >= N) || (y >= N) || (y > x)) return;
                int pos = y*N+x;
                int my_diag = x-y;
                diags[my_diag * N + x] = matrix[pos];
            }
            """
        )
        self.extract_diags_kernel = mod.get_function("extract_upper_diags")
        self._blocks = (32, 32, 1)
        self._grid = (updiv(self.n_frames, self._blocks[0]), updiv(self.n_frames, self._blocks[1]), 1)
        self.d_diags = garray.zeros((self.n_frames, self.n_frames), dtype=np.float32)
        self.d_sumdiags1 = garray.zeros(self.n_frames, dtype=np.float32)
        self.d_sumdiags2 = garray.zeros_like(self.d_sumdiags1)
        self._kern_args = [
            None,
            self.d_diags,
            np.int32(self.n_frames),
        ]

    def sum_diagonals(self, d_arr, d_out):
        self.d_diags.fill(0)
        self._kern_args[0] = d_arr.gpudata
        self.extract_diags_kernel(*self._kern_args, grid=self._grid, block=self._blocks)
        skmisc.sum(self.d_diags, axis=1, out=d_out)

    def _correlate_matmul_cublas(self, frames_flat, mask, calc_std=False, calc_ttcf=False):
        arr = np.ascontiguousarray(frames_flat[:, mask], dtype=np.float32)
        npix = arr.shape[1]
        # Pre-allocating memory for all bins might save a bit of time,
        # but would take more memory
        d_arr = garray.to_gpu(arr)
        d_outer = cublas.dot(d_arr, d_arr, transb="T", handle=self.cublas_handle)
        d_means = skmisc.mean(d_arr, axis=1, keepdims=True)
        d_denom_mat = cublas.dot(d_means, d_means, transb="T", handle=self.cublas_handle)

        self.sum_diagonals(d_outer, self.d_sumdiags1)
        self.sum_diagonals(d_denom_mat, self.d_sumdiags2)
        self.d_sumdiags1 /= self.d_sumdiags2
        self.d_sumdiags1 /= npix

        ttcf = d_outer.get() / d_denom_mat.get() if calc_ttcf else None  # TODO check
        std = None

        return self.d_sumdiags1.get(), std, ttcf

    def correlate(self, frames, calc_std=False, calc_ttcf=False):
        result = self.get_correlation_result(calc_std=calc_std, calc_ttcf=calc_ttcf)

        frames_flat = frames.reshape((self.n_frames, -1))
        for i, bin_val in enumerate(self.bins):
            mask = self.qmask.ravel() == bin_val
            g2, std, ttcf = self._correlate_matmul_cublas(frames_flat, mask, calc_std=calc_std, calc_ttcf=calc_ttcf)
            result.correlation[i] = g2
            if calc_std:
                result.std[i] = std
            if calc_ttcf:
                result.ttcf[i] = ttcf
        return result
