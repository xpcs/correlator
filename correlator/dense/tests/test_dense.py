import numpy as np
import pytest

from correlator.dense.matmul import MatMulCorrelator, py_dense_correlator
from correlator.testutils import XPCSDataset

try:
    from correlator.dense.fft_cuda import VKFFTCorrelator, cuda, vkfft_cuda
    from correlator.dense.matmul_cuda import CublasMatMulCorrelator, cublas

    cuda = True
except (ImportError, OSError):
    cuda = False
    vkfft_cuda = None
try:
    import scipy
except ImportError:
    scipy = None

if scipy:
    from correlator.dense.fft import FFTCorrelator


mae = lambda x: np.nanmax(np.abs(x))

# see testutils
datasets_to_test = ["eiger_514_10k"]


@pytest.fixture(scope="class")
def bootstrap(request):
    cls = request.cls

    cls.datasets = {dataset_name: XPCSDataset(dataset_name) for dataset_name in datasets_to_test}
    cls.ref = dict.fromkeys(datasets_to_test, None)
    cls.tol_g2 = 5e-3
    cls.tol_std = 1e-5
    if cuda and cublas:
        import pycuda.autoinit
    # Crop to lesser number of frames
    for _, d in cls.datasets.items():
        d.data = d.data[:1000]


@pytest.mark.usefixtures("bootstrap")
class TestDense:
    def get_reference_correlation(self, dataset_name):
        if self.ref[dataset_name] is not None:
            return self.ref[dataset_name]
        dataset = self.datasets[dataset_name]
        ref = []
        for bin_val in range(1, dataset.dataset_desc.bins + 1):
            mask = dataset.qmask == bin_val
            ref.append(py_dense_correlator(dataset.data, mask, calc_std=True, calc_ttcf=True))
        self.ref[dataset_name] = ref
        return ref

    def compare(self, dataset_name, res, method_name):
        dataset = self.datasets[dataset_name]
        ref = self.get_reference_correlation(dataset_name)

        for bin_idx in range(dataset.dataset_desc.bins):
            err_max_g2 = mae(res.correlation[bin_idx] - ref[bin_idx].correlation)

            print("%s: err_max(g2) = %.2e" % (method_name, err_max_g2))
            assert err_max_g2 < self.tol_g2, (
                "Something wrong with %s on dataset %s at bin idx %d: err_max(g2) = %.2e"
                % (
                    method_name,
                    dataset_name,
                    bin_idx,
                    err_max_g2,
                )
            )
            if res.std is None:
                continue
            err_max_std = mae(res.std[bin_idx] - ref[bin_idx].std)
            print("%s: err_max(std) = %.2e" % (method_name, err_max_std))
            assert err_max_std < self.tol_std, (
                "Something wrong with %s on dataset %s at bin idx %d: err_max(std) = %.2e"
                % (
                    method_name,
                    dataset_name,
                    bin_idx,
                    err_max_std,
                )
            )

    def test_matmul_correlator(self):
        for dataset_name in datasets_to_test:
            dataset = self.datasets[dataset_name]
            n_frames, ny, nx = dataset.data.shape
            correlator = MatMulCorrelator(n_frames, dataset.qmask)
            res = correlator.correlate(dataset.data, calc_std=True, calc_ttcf=True)
            self.compare(dataset_name, res, "Matmul correlator")

    @pytest.mark.skipif(cuda is None, reason="Need pycuda + scikit-cuda for this test")
    def test_cuda_matmul_correlator(self):
        for dataset_name in datasets_to_test:
            dataset = self.datasets[dataset_name]
            n_frames, ny, nx = dataset.data.shape
            correlator = CublasMatMulCorrelator(n_frames, dataset.qmask)
            res = correlator.correlate(dataset.data, calc_std=False, calc_ttcf=True)
            self.compare(dataset_name, res, "Cublas Matmul correlator")

    @pytest.mark.skipif(scipy is None, reason="need scipy for this test")
    def test_fft_dense_correlator(self):
        for dataset_name in datasets_to_test:
            dataset = self.datasets[dataset_name]
            n_frames, ny, nx = dataset.data.shape
            fft_correlator = FFTCorrelator(n_frames, dataset.qmask)
            res = fft_correlator.correlate(dataset.data)
            self.compare(dataset_name, res, "FFT correlator")

    @pytest.mark.skipif(cuda is None or vkfft_cuda is None, reason="need pycuda and pyvkfft for this test")
    def test_cuda_fft_dense_correlator(self):
        for dataset_name in datasets_to_test:
            dataset = self.datasets[dataset_name]
            n_frames, ny, nx = dataset.data.shape
            fft_correlator = VKFFTCorrelator(n_frames, dataset.qmask)
            res = fft_correlator.correlate(dataset.data)
            self.compare(dataset_name, res, "VKFFT correlator")
