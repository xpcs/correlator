import numpy as np

from ..utils import updiv
from .fft import FFTCorrelator

try:
    import pycuda.gpuarray as garray
    from pycuda.compiler import SourceModule

    cuda = True
except ImportError:
    cuda = None

try:
    from pyvkfft.cuda import VkFFTApp as vkfft_cuda
except Exception:
    vkfft_cuda = None


class VKFFTCorrelator(FFTCorrelator):
    def _init_finalize(self):
        super()._init_finalize()
        if cuda is None or vkfft_cuda is None:
            raise ImportError("Need pycuda and pyvkfft to use this class")
        # init VKFFT plans
        self.vkfft_plans = {}
        for i, bin_val in enumerate(self.bins):
            fft_padded_shape = (self.n_frames + self.pad_length, (self.qmask == bin_val).sum())
            self.vkfft_plans[i] = vkfft_cuda(
                fft_padded_shape,
                dtype=np.complex64,
                ndim=1,
                inplace=False,
                r2c=False,
                axes=0,
            )

        # Dedicated kernel for summing the result of IFFT(x1 * x2), which is a complex array, along horizontal axis.
        # skcuda.sum() does not work for some reason, and skcuda.dot() entails too many intermediate arrays.
        self._sum_along_axis_mod = SourceModule(
            """
            #include <pycuda-complex.hpp>
            typedef pycuda::complex<float> complex;

            __global__ void horizontal_sum_subset(complex* arr2D, float* res_arr1D, int width, int x_start, int y_start, int x_stop, int y_stop) {
                uint y = blockDim.x * blockIdx.x + threadIdx.x;
                y += y_start;
                if (y >= y_stop) return;
                float res = 0.0f;
                for (uint x = x_start; x < x_stop; x++) res += arr2D[y * width + x]._M_re;
                res_arr1D[y - y_start] = res;
            }
            """,
        )
        self._sum_along_axis = self._sum_along_axis_mod.get_function("horizontal_sum_subset")
        self._sum_along_axis.prepare("PPiiiii", [])

    def correlate(self, frames):
        result = self.get_correlation_result(calc_std=False, calc_ttcf=False)
        frames_flat = frames.reshape((self.n_frames, -1))
        for i, bin_val in enumerate(self.bins):
            mask = (self.qmask == bin_val).ravel()
            # TODO speed-up this compaction step, either multithread or on GPU
            frames_flat_currbin = frames_flat[:, mask]
            n_pix = frames_flat_currbin.shape[-1]
            #

            # Pad into complex data type (VKFFT does not support "axis=0" with R2C)
            # TODO perform padding on GPU
            frames1 = np.ascontiguousarray(
                np.pad(frames_flat_currbin, ((self.pad_length, 0), (0, 0)), mode="constant"), dtype=np.complex64
            )
            frames2 = np.ascontiguousarray(
                np.pad(frames_flat_currbin[::-1, :], ((self.pad_length, 0), (0, 0)), mode="constant"),
                dtype=np.complex64,
            )

            d_frames1 = garray.to_gpu(frames1)
            d_out1 = garray.zeros_like(d_frames1)
            d_frames2 = garray.to_gpu(frames2)
            d_out2 = garray.zeros_like(d_out1)

            self.vkfft_plans[i].fft(d_frames1, dest=d_out1)
            self.vkfft_plans[i].fft(d_frames2, dest=d_out2)

            d_out1 *= d_out2
            self.vkfft_plans[i].ifft(d_out1, dest=d_frames1)

            # sum(d_frames1, axis=-1)
            # There are still relatively large numerical discrepancies
            # between this implementation and the reference (double-precision) one.
            d_a3 = garray.zeros(self.n_frames, dtype=np.float32)
            self._sum_along_axis(
                d_frames1.gpudata,
                d_a3.gpudata,
                np.int32(n_pix),  # width
                np.int32(0),  # x_start
                np.int32(self.n_frames - 1),  # y_start
                np.int32(n_pix),  # x_stop
                np.int32(2 * self.n_frames - 1),  # y_stop
                block=(1024, 1, 1),
                grid=(updiv(self.n_frames, 1024), 1, 1),
            )

            # TODO perform denominator correlation on GPU ? Not sure about the gain
            a4 = frames_flat_currbin.sum(axis=1).astype(np.float64)
            a5 = (self._corr_fft_1d(a4) / (mask.sum()))[self.n_frames - 1 : 2 * self.n_frames - 1]

            result.correlation[i] = d_a3.get() / a5

        return result
