from math import sqrt

import numpy as np

from ..base import BaseCorrelator, CorrelationResult
from ..utils import tinker_ttcf_diagonal


def py_dense_correlator(xpcs_data, mask, calc_std=False, calc_ttcf=False, tinker_diagonal=False, scale_factor=None):
    """
    Reference implementation of the dense correlator.

    Parameters
    -----------
    xpcs_data: numpy.ndarray
        Stack of XPCS frames with shape (n_frames, n_rows, n_columns)
    mask: numpy.ndarray
        Mask of bins in the format (n_rows, n_columns).
        Zero pixels indicate unused pixels.
    calc_std: boolean
        Calculate the standard deviation in addition to the mean

    Return: 1 or 2 arrays depending on `calc_std`
    """
    ind = np.where(mask > 0)  # unused pixels are 0
    xpcs_data = np.array(xpcs_data[:, ind[0], ind[1]], np.float32)  # (n_tau, n_pix)
    meanmatr = np.mean(xpcs_data, axis=1)  # xpcs_data.sum(axis=-1).sum(axis=-1)/n_pix
    ltimes, lenmatr = np.shape(xpcs_data)  # n_tau, n_pix
    meanmatr.shape = 1, ltimes

    num = np.dot(xpcs_data, xpcs_data.T)
    denom = np.dot(meanmatr.T, meanmatr)

    res = np.zeros(ltimes)
    dev = None
    ttcf = None
    if calc_std:
        dev = np.zeros_like(res)
    if calc_ttcf:
        ttcf = num / denom  # TODO handle nan, and check normalization

    for i in range(ltimes):
        dia_n = np.diag(num, k=i) / lenmatr
        dia_d = np.diag(denom, k=i)
        res[i] = np.sum(dia_n) / np.sum(dia_d)
        if calc_std:
            dev[i] = np.std(dia_n / dia_d) / sqrt(len(dia_d))

    if calc_ttcf and tinker_diagonal:
        tinker_ttcf_diagonal(ttcf, scale_factor=scale_factor)

    return CorrelationResult(correlation=res, std=dev, ttcf=ttcf)


class MatMulCorrelator(BaseCorrelator):
    def correlate(self, frames, calc_std=False, calc_ttcf=False, tinker_diagonal=False):
        result = self.get_correlation_result(calc_std=calc_std, calc_ttcf=calc_ttcf)
        for i, bin_value in enumerate(self.bins):
            res_onebin = py_dense_correlator(
                frames,
                self.qmask == bin_value,
                calc_std=calc_std,
                calc_ttcf=calc_ttcf,
                tinker_diagonal=tinker_diagonal,
                scale_factor=self.scale_factors[i + 1],
            )
            result.correlation[i] = res_onebin.correlation
            if calc_std:
                result.std[i] = res_onebin.std
            if calc_ttcf:
                result.ttcf[i] = res_onebin.ttcf
        return result
