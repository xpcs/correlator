import numpy as np
from scipy.fft import irfft, rfft
from silx.image.tomography import get_next_power

from ..base import BaseCorrelator
from ..utils import get_available_threads


class FFTCorrelator(BaseCorrelator):
    def _init_finalize(self):
        self.pad_length = get_next_power(self.n_frames * 2) - self.n_frames

    def _corr_fft_1d(self, arr, n_threads=None):
        arr_p1 = np.pad(arr, (self.pad_length, 0))
        arr_p2 = np.pad(arr[::-1], (self.pad_length, 0))
        arr_f1 = rfft(arr_p1, workers=n_threads)
        arr_f2 = rfft(arr_p2, workers=n_threads)
        return irfft(arr_f1 * arr_f2, workers=n_threads).real

    def correlate(self, frames, n_threads=None):
        n_threads = n_threads or get_available_threads()

        result = self.get_correlation_result(calc_std=False, calc_ttcf=False)
        frames_flat = frames.reshape((self.n_frames, -1))
        for i, bin_val in enumerate(self.bins):
            mask = (self.qmask == bin_val).ravel()
            frames_flat_currbin = frames_flat[:, mask]

            a1 = rfft(
                np.pad(frames_flat_currbin, ((self.pad_length, 0), (0, 0)), mode="constant"), axis=0, workers=n_threads
            )
            a2 = rfft(
                np.pad(frames_flat_currbin[::-1, :], ((self.pad_length, 0), (0, 0)), mode="constant"),
                axis=0,
                workers=n_threads,
            )
            a3 = irfft(a1 * a2, axis=0, workers=n_threads).real.sum(axis=-1)

            # cast to double here, otherwise scipy fft lacks accuracy, which is seen on the end result
            a4 = frames_flat_currbin.sum(axis=1).astype(np.float64)
            a5 = self._corr_fft_1d(a4) / (mask.sum())

            result.correlation[i] = (a3 / a5)[self.n_frames - 1 : 2 * self.n_frames - 1]

        return result
