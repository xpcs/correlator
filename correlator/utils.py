import os
from functools import partial

import numpy as np


def get_folder_path(foldername=""):
    _file_dir = os.path.dirname(os.path.realpath(__file__))
    package_dir = _file_dir
    return os.path.join(package_dir, foldername)


def get_available_threads():
    try:
        n_threads = len(os.sched_getaffinity(0))
    except AttributeError:
        n_threads = int(os.environ.get("SLURM_CPUS_PER_TASK", os.cpu_count()))
    return n_threads


def updiv(a, b):
    """
    return the integer division, plus one if `a` is not a multiple of `b`
    """
    return (a + (b - 1)) // b


def _docstring(dest, origin):
    """Implementation of docstring decorator.

    It patches dest.__doc__.
    """
    if not isinstance(dest, type) and isinstance(origin, type):
        # func is not a class, but origin is, get the method with the same name
        try:
            origin = getattr(origin, dest.__name__)
        except AttributeError:
            raise ValueError("origin class has no %s method" % dest.__name__)

    dest.__doc__ = origin.__doc__
    return dest


def docstring(origin):
    """Decorator to initialize the docstring from another source.

    This is useful to duplicate a docstring for inheritance and composition.

    If origin is a method or a function, it copies its docstring.
    If origin is a class, the docstring is copied from the method
    of that class which has the same name as the method/function
    being decorated.

    :param origin:
        The method, function or class from which to get the docstring
    :raises ValueError:
        If the origin class has not method n case the
    """
    return partial(_docstring, origin=origin)


def binning(img, bin_factor, out_dtype=np.float32, normalize=True):
    """
    Bin an image by a factor of "bin_factor".

    Parameters
    ----------
    bin_factor: tuple of int
        Binning factor in each axis.
    out_dtype: dtype, optional
        Output data type. Default is float32.

    Notes
    -----
    If the image original size is not a multiple of the binning factor,
    the last items (in the considered axis) will be dropped.
    The resulting shape is (img.shape[0] // bin_factor[0], img.shape[1] // bin_factor[1])
    """
    s = img.shape
    n0, n1 = bin_factor
    shp = (s[0] - (s[0] % n0), s[1] - (s[1] % n1))
    sub_img = img[: shp[0], : shp[1]]
    out_shp = (shp[0] // n0, shp[1] // n1)
    res = np.zeros(out_shp, dtype=out_dtype)
    for i in range(n0):
        for j in range(n1):
            res[:] += sub_img[i::n0, j::n1]
    if normalize:
        res /= n0 * n1
    return res


def tinker_ttcf_diagonal(ttcf, scale_factor=None):
    """
    Function that modifies the main diagonal of two-times correlation function (TTCF)
    """
    v = np.diag(ttcf, k=1)
    mean_value = np.mean(v[v > 0])
    ttcf[np.diag_indices(ttcf.shape[0])] = mean_value
    if scale_factor is not None:
        ttcf *= scale_factor
    return ttcf


def tinker_ttcf_diagonal_line_layout(ttcf, scale_factor=None):
    """
    Function that modifies the main diagonal of two-times correlation function (TTCF)
    """
    v = ttcf[:, 1]
    mean_value = np.mean(v[v > 0])
    ttcf[:, 0] = mean_value
    if scale_factor is not None:
        ttcf *= scale_factor
    return ttcf
