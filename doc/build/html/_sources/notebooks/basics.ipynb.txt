{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "07ee4758-44ee-4431-8ec5-02e4ec39ecdb",
   "metadata": {},
   "source": [
    "# Basic tutorial to XPCS correlator "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9ec16959-f11e-484f-a052-22084ba28838",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Warning: log-rebinning activated, setting integer intermediate results data type to 64 bits\n",
      "Warning: log-rebinning activated, setting integer intermediate results data type to 64 bits\n"
     ]
    }
   ],
   "source": [
    "import sys\n",
    "import os\n",
    "import numpy as np\n",
    "\n",
    "from correlator.testutils import load_xpcs_compacted_data\n",
    "from correlator.sparse.time_opencl import SpaceToTimeCompaction\n",
    "from correlator.sparse.space import SpaceCompactedData\n",
    "from correlator.event.event_matrix import TMatrixEventCorrelator\n",
    "from correlator.event.event_matrix import SMatrixEventCorrelator\n",
    "\n",
    "\n",
    "sys.path.append(os.path.abspath('../'))\n",
    "\n",
    "\n",
    "# Define the path to the filename and qmask file. In this case is our test data\n",
    "data_fname=os.path.join('/scisoft/dynamix/data/dataset02/xpcs_010000.npz')\n",
    "qmask_fname=os.path.join('/data/id10/inhouse/software/dynamix/datasets/dataset02/analysis/scan0005_0_10000/DUKE_qmask.npy')\n",
    "\n",
    "# Loading data from the file and qmask\n",
    "data, pix_idx, offset, qmask, frame_shape = load_xpcs_compacted_data(\n",
    "            data_fname, qmask_fname=qmask_fname, n_frames=1000) # In this dataset we have 10k frames \n",
    "\n",
    "s_sparse_data = SpaceCompactedData(data=data,indices=pix_idx, offsets=offset, frame_shape=frame_shape)\n",
    "n_frames = s_sparse_data.offsets.size - 1\n",
    "\n",
    "\n",
    "n_frames=np.int32(n_frames) # !!!! Here we need to be sure that n_frames is int32 \n",
    "                            # Otherwise there will be an error for n_frame > int8\n",
    "\n",
    "devicetype='GPU' # or CPU / GPU or ALL (automatic choice) #If you use CPU with log10 binning you \n",
    "                 #need to be sure that opencl installed on the machine supports 64 bit atomic opperations\n",
    "\n",
    "# Convert from \"space-sparse\" to \"time-sparse\" on GPU\n",
    "space_to_time = SpaceToTimeCompaction(s_sparse_data.frame_shape, dtype=np.uint8,devicetype=devicetype)\n",
    "d_t_sparse_data = space_to_time.space_compact_to_time_compact(s_sparse_data)\n",
    "\n",
    "# Set linear binning iof one of the axis\n",
    "binning=1\n",
    "\n",
    "# Calculate ttcf time format, if horizontal_binning=None, there will be no binning\n",
    "correlator_time = TMatrixEventCorrelator(n_frames, qmask,binning=binning,horizontal_binning='log10',devicetype=devicetype) \n",
    "res_t = correlator_time.correlate(d_t_sparse_data, normalize_ttcf_on_device=True, return_numpy_arrays=False)\n",
    "\n",
    "#Calculate ttfc space format\n",
    "correlator_space = SMatrixEventCorrelator(n_frames, qmask,binning=binning,horizontal_binning='log10',devicetype=devicetype) \n",
    "res_s = correlator_space.correlate(s_sparse_data, normalize_ttcf_on_device=True, return_numpy_arrays=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "86ab321c-6f41-43f6-9935-1f9fb561d9f7",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(np.int8(5), 32)\n",
      "(np.int8(5), 32)\n",
      "(5, 1000, 32)\n"
     ]
    }
   ],
   "source": [
    "# Here is and output of the calculation\n",
    "print(res_t.correlation.shape)\n",
    "print(res_t.std.shape)\n",
    "print(res_t.ttcf.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "029ca1a8-30e3-47d8-bc58-68e0fc452fb7",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "xpcs",
   "language": "python",
   "name": "xpcs"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
