XPCS Introduction
=================

Overview
--------

X-ray Photon Correlation Spectroscopy (XPCS) is a  technique used to study the dynamics of materials at the nanoscale. \
It leverages the coherent properties of synchrotron radiation to measure the temporal fluctuations in the scattered intensity of X-rays.

Applications
------------

XPCS is widely used in various fields, including:

- Soft matter physics
- Materials science
- Biology
- Nanotechnology

Key Concepts
------------

- **Coherent X-rays**: Essential for observing interference patterns.
- **Scattering**: Interaction of X-rays with the sample.
- **Correlation function**: Mathematical tool to analyze the temporal fluctuations.

References
----------

For more detailed information, refer to the following sources:

- [1] Cipelletti, L., Brambilla, G., Maccarrone, S., Caroff, S., "Simultaneous measurement of the microscopic dynamics and the mesoscopic displacement field in soft systems by speckle imaging," *Opt. Express*, vol. 21, pp. 22353, 2013. https://doi.org/10.1364/OE.21.022353
- [2] Cipelletti, L., Weitz, D.A., "Ultralow-angle dynamic light scattering with a charge coupled device camera based multispeckle, multitau correlator," *Review of Scientific Instruments*, vol. 70, pp. 3214–3221, 1999. https://doi.org/10.1063/1.1149894
- [3] Duri, A., Bissig, H., Trappe, V., Cipelletti, L., "Time-resolved-correlation measurements of temporally heterogeneous dynamics," *Phys. Rev. E*, vol. 72, 051401, 2005. https://doi.org/10.1103/PhysRevE.72.051401
- [4] Lumma, D., Lurio, L.B., Mochrie, S.G.J., Sutton, M., "Area detector based photon correlation in the regime of short data batches: Data reduction for dynamic x-ray scattering," *Review of Scientific Instruments*, vol. 71, pp. 3274–3289, 2000. https://doi.org/10.1063/1.1287637
- [5] Nakaye, Y., Sakumura, T., Sakuma, Y., Mikusu, S., Dawiec, A., Orsini, F., Grybos, P., Szczygiel, R., Maj, P., Ferrara, J.D., Taguchi, T., "Characterization and performance evaluation of the XSPA-500k detector using synchrotron radiation," *J Synchrotron Rad*, vol. 28, pp. 439–447, 2021. https://doi.org/10.1107/S1600577520016665
- [6] Zhang, Q., Dufresne, E.M., Nakaye, Y., Jemian, P.R., Sakumura, T., Sakuma, Y., Ferrara, J.D., Maj, P., Hassan, A., Bahadur, D., Ramakrishnan, S., Khan, F., Veseli, S., Sandy, A.R., Schwarz, N., Narayanan, S., "20 µs-resolved high-throughput X-ray photon correlation spectroscopy on a 500k pixel detector enabled by data-management workflow," *J Synchrotron Rad*, vol. 28, pp. 259–265, 2021. https://doi.org/10.1107/S1600577520014319
