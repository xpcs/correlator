.. _xpcs_calc_basics:

Calculation Basics
========================

In this section, we will cover the fundamental mathematical concepts underlying XPCS. \
Currently the best source is Piere Palleo presentation on XPCS calculations.

You can find the detailed presentation here: 

    .. raw:: html

        <a href="https://cloud.esrf.fr/s/ArZfGTJJoodpcWe">Piere Palleo presentation on XPCS calculations</a>
    




