correlator.event package
========================

Submodules
----------

correlator.event.event module
-----------------------------

.. automodule:: correlator.event.event
   :members:
   :undoc-members:
   :show-inheritance:

correlator.event.event\_matrix module
-------------------------------------

.. automodule:: correlator.event.event_matrix
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: correlator.event
   :members:
   :undoc-members:
   :show-inheritance:
