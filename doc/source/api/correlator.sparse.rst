correlator.sparse package
=========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   correlator.sparse.tests

Submodules
----------

correlator.sparse.space module
------------------------------

.. automodule:: correlator.sparse.space
   :members:
   :undoc-members:
   :show-inheritance:

correlator.sparse.time module
-----------------------------

.. automodule:: correlator.sparse.time
   :members:
   :undoc-members:
   :show-inheritance:

correlator.sparse.time\_opencl module
-------------------------------------

.. automodule:: correlator.sparse.time_opencl
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: correlator.sparse
   :members:
   :undoc-members:
   :show-inheritance:
