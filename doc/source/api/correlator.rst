correlator package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   correlator.cuda
   correlator.dense
   correlator.event
   correlator.opencl
   correlator.sparse

Submodules
----------

correlator.base module
----------------------

.. automodule:: correlator.base
   :members:
   :undoc-members:
   :show-inheritance:

correlator.testutils module
---------------------------

.. automodule:: correlator.testutils
   :members:
   :undoc-members:
   :show-inheritance:

correlator.utils module
-----------------------

.. automodule:: correlator.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: correlator
   :members:
   :undoc-members:
   :show-inheritance:
