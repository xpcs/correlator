correlator.opencl package
=========================

Submodules
----------

correlator.opencl.correlator module
-----------------------------------

.. automodule:: correlator.opencl.correlator
   :members:
   :undoc-members:
   :show-inheritance:

correlator.opencl.processing module
-----------------------------------

.. automodule:: correlator.opencl.processing
   :members:
   :undoc-members:
   :show-inheritance:

correlator.opencl.utils module
------------------------------

.. automodule:: correlator.opencl.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: correlator.opencl
   :members:
   :undoc-members:
   :show-inheritance:
