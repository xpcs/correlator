Installation
============

To install the package from GitLab, you can use `pip`. Follow the steps below:

1. Clone the repository:

    ```
    git clone https://gitlab.esrf.fr/xpcs/correlator
    ```

2. Navigate to the project directory:

    ```
    cd your-project
    ```

3. Install the package using `pip`:

    ```
    pip install .
    ```

Alternatively, you can install directly from the GitLab repository:

    ```
    pip install git+https://gitlab.esrf.fr/xpcs/correlator.git
    ```

